package com.projekt_licencjat.licencjat;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.projekt_licencjat.licencjat.models.*;
import com.projekt_licencjat.licencjat.repositories.*;
import com.projekt_licencjat.licencjat.util.JwtUtil;
import com.projekt_licencjat.licencjat.services.MyUserDetailsService;
import javassist.bytecode.ByteArray;
import org.hibernate.annotations.TypeDef;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.swing.plaf.basic.BasicInternalFrameTitlePane;
import java.awt.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class RestResource {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private MyUserDetailsService userDetailsService;

    @Autowired
    private JwtUtil jwtTokenUtil;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private RankingRepository rankingRepo;

    @Autowired
    private MissionRepository missionRepo;

    @Autowired
    private UserMissionRepository userMissionRepo;

    @Autowired
    private ObjectiveRepository objectiveRepo;

    @Autowired
    private ObjectiveRepoTransaction objectiveRepoTransaction;

    @Autowired
    private ProgressMissionsRepository progressMissionsRepo;

    @Autowired
    private RankingUserRepository rankingUserRepo;


    @RequestMapping("/userdata")
    @ResponseBody
    public User userdata(Authentication authentication){
        return userRepo.findByEmail(authentication.getName());
    }


    @RequestMapping("/getMission")
    @ResponseBody
    public MissionResponse getMission(@RequestParam int userId, @RequestParam int missionId){
        return missionRepo.getMission(userId, missionId);
    }

    @RequestMapping("/getObjective")
    @ResponseBody
    public ObjectiveResponse getCurrentObjective(@RequestParam int missionId, Authentication authentication){
        int current_season = 0;
        int missionProgress;
        User userData = userRepo.findByEmail(authentication.getName());
        Ranking ranking = rankingRepo.getUserRankingBySeason(current_season, userData.getUser_id());

        ProgressMissions progressMissionObj = progressMissionsRepo.getMissionsProgress(userData.getUser_id(), missionId);

        String responseText = "";
        Boolean success;


        if(progressMissionObj == null){
            missionProgress = 1;
            int mission_points = missionRepo.getMinimumPoints(missionId);
            if(mission_points <= ranking.getPoints()) {
                progressMissionsRepo.addProgress(userData.getUser_id(), missionId);
                success = true;
                responseText = "successfully started new mission";
            }
            else{
                success = false;
                responseText = "user doesn't have enough points to start mission";
            }
        }
        else {
            if(progressMissionObj.getStatus().equals("finished")) {
                success = true;
                progressMissionsRepo.replayMission(progressMissionObj.getProgress_missions_id());
                missionProgress = 1;
                responseText = "successfully started replaying mission";
            }
            else {
                Mission mission = missionRepo.getMission(userData.getUser_id(), missionId).getMission();
                missionProgress = progressMissionObj.getObjective_progress();
                if(mission.getObjective_count() < missionProgress){
                    success = false;
                    responseText = "finished_mission";
                    finishMission(progressMissionObj.getProgress_missions_id(),progressMissionObj.getStatus(),userData.getUser_id(), mission.getPoints());
                }
                else{
                    success = true;
                    responseText = "successfully started continuing mission";
                }
            }

        }

        Objective objective;
        if(success == true)
            objective = objectiveRepo.getCurrentObjective(missionId, missionProgress);
        else
            objective = null;

        return new ObjectiveResponse(success, responseText, objective);

    }

    void finishMission(Integer progressId,String status, Integer userId, Integer missionPoints){

        progressMissionsRepo.finishMission(progressId);
        if(status.equals("active"))
            rankingRepo.giveUserPoints(userId, missionPoints);
    }

    @RequestMapping("/addMission")
    @ResponseBody
    public MissionCreatorResponse addMission(@RequestPart("mission") String mission, @RequestPart("image") MultipartFile file,
                                      Authentication authentication,  Errors caughtErrors) throws IOException, SQLException {

        final Integer objectivePoints = 50;

        MissionCreatorErrors errors = new MissionCreatorErrors();
        Boolean success = true;
        Integer mission_id = null;

        User user = userRepo.findByEmail(authentication.getName());

        FileInputStream inputStream = null;
        byte[] imageByte = {};
        ObjectMapper mapper = new ObjectMapper();
        MissionCreatorObject missionObj = mapper.readValue(mission, MissionCreatorObject.class);

        Integer numOfObjectives =  missionObj.getObjectives().size();


        if(hasEmptyFields(missionObj)) {
            success = false;
            errors.setEmptyField(true);
        }

        if(file.getSize() > 1048576){
            success = false;
            errors.setImageTooLarge(true);
            caughtErrors.rejectValue("image", "ImageTooLarge");
        }
        else{
            if(file.getSize() > 10){
                inputStream = (FileInputStream)file.getInputStream();
                imageByte = inputStream.readAllBytes();
            }
        }

        if(numOfObjectives == 0) {
            success = false;
            errors.setEmptyField(true);
            caughtErrors.reject("NotEmpty");
        }

        Integer difficulty = missionObj.getMissionDifficulty();

        if(difficulty <1 && difficulty > 5){
            success = false;
            errors.setWrongDifficulty(true);
        }

        if(missionObj.getDescription().length() > 300){
            success = false;
            errors.setTooLongDescription(true);
        }

        if(missionObj.getName().length() > 50){
            success = false;
            errors.setTooLongName(true);
        }

        if(missionObj.getMinimumLvl() != null) {
            if (missionObj.getMinimumLvl() < 0 || missionObj.getMinimumLvl() > 100) {
                success = false;
                errors.setWrongLvl(true);
            }
        }

        if(success){
            Double multiplier = Double.valueOf(1+(1-(1/difficulty)));
            Integer calculatedPoints = (int)Math.floor((objectivePoints * multiplier) * numOfObjectives);

            Integer minimalPoints = this.calculateMinimumPoints(missionObj.getMinimumLvl());

            Integer createdMissionId = missionRepo.addMission(missionObj.getName(), missionObj.getDescription(), user.getUser_id(),
                    imageByte, calculatedPoints, true, numOfObjectives, missionObj.getMissionDifficulty(), minimalPoints);


            Boolean objSuccess = objectiveRepoTransaction.insertObjective(createdMissionId, missionObj.getObjectives());

            if(!objSuccess){
                missionRepo.deleteMission(createdMissionId);
                success = false;
                errors.setObjectiveError(true);
             }
            else{
                mission_id = createdMissionId;
            }

        }

        return new MissionCreatorResponse(success, errors, mission_id);
    }

    boolean hasEmptyFields(MissionCreatorObject mission){
        return mission.getDescription().isEmpty() || mission.getMinimumLvl()  < 0 || mission.getName().isEmpty() || mission.getMinimumLvl() == null;
    }

    MissionCreatorErrors checkMissionCreatorErrors(Errors errors){
        MissionCreatorErrors missionCreatorErrors = new MissionCreatorErrors();
        for (ObjectError error: errors.getAllErrors()) {
            switch(error.getCode()){
                case "NotEmpty":
                    missionCreatorErrors.setEmptyField(true);
                    break;
                case "ImageTooLarge":
                    missionCreatorErrors.setImageTooLarge(true);
                    break;
                case "WrongValue":
                    missionCreatorErrors.setWrongDifficulty(true);
                    break;
                case "TooLongDescription":
                    missionCreatorErrors.setTooLongDescription(true);
                    break;
                case "TooLongName":
                    missionCreatorErrors.setTooLongName(true);
                    break;
                case "MinimumLvl":
                    missionCreatorErrors.setWrongLvl(true);
                    break;
            }
        }


        return missionCreatorErrors;
    }


    Integer calculateMinimumPoints(Integer minimumLvl){
        if(minimumLvl <= 0){
            return 1;
        }

        int pointsReq = 100;
        for(int i = 2; i < minimumLvl; i++){
            if(minimumLvl < 5)
                pointsReq += pointsReq * 2;
            else
                pointsReq +=pointsReq * 1.5;
        }

        return pointsReq;

    }

    @RequestMapping("/getUserProfile")
    @ResponseBody
    public ProfileResponse getUserProfile(Authentication authentication){
        int current_season = 0;

        User userData = userRepo.findByEmail(authentication.getName());
        Ranking ranking = rankingRepo.getUserRankingBySeason(current_season, userData.getUser_id());
        Integer finishedMissions = missionRepo.getCountFinishedMissions(userData.getUser_id());
        Integer repeated_missions = missionRepo.getCountReplayMissions(userData.getUser_id());

        finishedMissions += repeated_missions;

        return new ProfileResponse(userData, ranking, finishedMissions);
    }

    @RequestMapping("/getUserMissions")
    @ResponseBody
    public UserMissionsResponse getUserMissions(Authentication authentication){
        int current_season = 0;

        User userData = userRepo.findByEmail(authentication.getName());
        Ranking ranking = rankingRepo.getUserRankingBySeason(current_season, userData.getUser_id());
        List<UserMission> available = null;
        List<UserMission> in_progress = userMissionRepo.getUserMissions(userData.getUser_id(), "active");
        List<UserMission> replay = userMissionRepo.getUserMissions(userData.getUser_id(), "replay");
        List<UserMission> finished = userMissionRepo.getUserMissions(userData.getUser_id(), "finished");
        if(userData.getRoles().equals("ROLE_USER")) {
            available = userMissionRepo.getUserAvaibleMissions(userData.getUser_id(), ranking.getPoints(), true);
        }
        else if(userData.getRoles().equals("ROLE_ADMIN")){
            available = userMissionRepo.getUserAvaibleMissions(userData.getUser_id(), ranking.getPoints(), false);
        }

        in_progress.addAll(replay);

        return new UserMissionsResponse(in_progress, available, finished);
    }

    @RequestMapping("/getRanking")
    @ResponseBody
    public RankingResponse getRanking(Authentication authentication){
        User userData = userRepo.findByEmail(authentication.getName());

        List<RankingUser> ranking = rankingUserRepo.getRanking();
        RankingUser user = rankingUserRepo.getUserRanking(userData.getUser_id());

        return new RankingResponse(ranking, user);
    }


    @RequestMapping("/authtest")//delete
    @ResponseBody
    public String authtest(Authentication authentication){
        return authentication.getName();
    }

    @RequestMapping(value = "/user/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception{

        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword())
            );
        }
        catch(BadCredentialsException e){
            throw new Exception("Incorrect username or password", e);
        }


        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());

        final String jwt = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(new AuthenticationResponse(jwt));
    }




}
