package com.projekt_licencjat.licencjat.controllers;

import com.projekt_licencjat.licencjat.models.AuthenticationRequest;
import com.projekt_licencjat.licencjat.models.UserRegistration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.context.request.WebRequest;

@Controller
public class LoginController {
    @GetMapping("/user/login")
    public String showRegistrationForm(WebRequest request, Model model) {
        AuthenticationRequest userDto = new AuthenticationRequest();
        model.addAttribute("user", userDto);
        return "login";
    }
}
