package com.projekt_licencjat.licencjat.controllers;

import com.projekt_licencjat.licencjat.models.RegistrationErrors;
import com.projekt_licencjat.licencjat.models.RegistrationResponse;
import com.projekt_licencjat.licencjat.models.User;
import com.projekt_licencjat.licencjat.models.UserRegistration;
import com.projekt_licencjat.licencjat.repositories.RankingRepository;
import com.projekt_licencjat.licencjat.services.UserService;
import com.projekt_licencjat.licencjat.validation.EmailExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;


import javax.naming.Binding;
import javax.validation.Valid;

@Controller
public class RegistrationController {
    @Autowired
    private UserService service;

    @Autowired
    private RankingRepository rankingRepo;

    @GetMapping("/user/registration")
    public String showRegistrationForm(WebRequest request, Model model) {
        UserRegistration userDto = new UserRegistration();
        model.addAttribute("user", userDto);
        return "registration";
    }

    @PostMapping("/user/registration")
    @ResponseBody
    public RegistrationResponse registerUserAccount(@RequestBody @Valid UserRegistration accountRegister,  Errors errors) {
        Boolean success;
        RegistrationErrors registrationErrors;

        User registered = new User();
        if (!errors.hasErrors()) {
            registered = createUserAccount(accountRegister, errors);
        }
        if (registered == null) {
            errors.rejectValue("email", "EmailTaken");
        }


        if(errors.getErrorCount() == 0) {
            success = true;
            rankingRepo.addUser(registered.getUser_id());
        }
        else{
            success = false;
        }

        registrationErrors = checkRegistrationErrors(errors);
        return new RegistrationResponse(success, registrationErrors);
    }

    RegistrationErrors checkRegistrationErrors(Errors result){
        RegistrationErrors errors = new RegistrationErrors();

        for (ObjectError error: result.getAllErrors()) {

            switch(error.getCode()){
                case "EmailTaken":
                    errors.setEmailTaken(true);
                    break;
                case "ValidEmail":
                    errors.setInvalidEmail(true);
                    break;
                case "PasswordMatches":
                    errors.setPasswordsDoNotMatch(true);
                    break;
                case "NotEmpty":
                    errors.setEmptyField(true);
                    break;
            }
        }

        return errors;
    }

    private User createUserAccount(UserRegistration accountRegister, Errors result) {
        User registered = null;
        try {
            registered = service.registerNewUserAccount(accountRegister);
        } catch (EmailExistsException e) {
            return null;
        }
        return registered;
    }
}
