package com.projekt_licencjat.licencjat.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.projekt_licencjat.licencjat.models.*;
import com.projekt_licencjat.licencjat.repositories.ObjectiveRepository;
import com.projekt_licencjat.licencjat.repositories.ProgressMissionsRepository;
import com.projekt_licencjat.licencjat.repositories.RankingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.*;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.util.HtmlUtils;

import javax.servlet.http.HttpSession;
import javax.swing.text.html.ObjectView;
import javax.websocket.Session;
import java.util.HashMap;
import java.util.Map;

@Controller
public class WebSocketController {

    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    @Autowired
    private ObjectiveRepository objectiveRepo;

    @Autowired
    private ProgressMissionsRepository progressMissionsRepo;



    @MessageMapping("/activity_1")
    @SendToUser("/topic/activity")
    public ServerMessage activity_1(@Payload UserActivityMessage_1 userActivity, SimpMessageHeaderAccessor headerAccessor) throws Exception {
        String responseType = "";
        Boolean responseTrue = false;

        Objective objective = (Objective)headerAccessor.getSessionAttributes().get("objective");
        String objectiveJson = objective.getObjective_json();
        ObjectMapper mapper = new ObjectMapper();

        ActivityType_1 objectiveActivity = mapper.readValue(objectiveJson, ActivityType_1.class);
        Long length = userActivity.getEnd() - userActivity.getStart();

        if(length < 0 || length < objectiveActivity.getLength()){
            responseTrue = false;
            responseType = "objective_cleared";
        }
        else{
            responseTrue = true;
            responseType = "objective_cleared";
            finishObjective(headerAccessor);
        }


        return new ServerMessage(responseType, responseTrue);
    }

    @MessageMapping("/activity_2")
    @SendToUser("/topic/activity")
    public ServerMessage activity_2(@Payload UserActivityMessage_2 userActivity, SimpMessageHeaderAccessor headerAccessor) throws Exception{
        final Integer maxOffset = 25;
        String responseType = "";
        Boolean responseTrue = false;

        Objective objective = (Objective)headerAccessor.getSessionAttributes().get("objective");
        String objectiveJson = objective.getObjective_json();
        ObjectMapper mapper = new ObjectMapper();

        ActivityType_2 objectiveActivity = mapper.readValue(objectiveJson, ActivityType_2.class);

        Double pos_x = objectiveActivity.getObject_coordinates()[0] - userActivity.getLongitude();
        Double pos_y = objectiveActivity.getObject_coordinates()[1] - userActivity.getLatitude();

        Integer user_angle = (userActivity.getCompass() + 270) % 360;

        Double perfect_angle = Math.toDegrees(Math.atan2(pos_y,pos_x));
        if(perfect_angle < 0)
            perfect_angle = (perfect_angle + 180);
        else
            perfect_angle = (360 - perfect_angle);

        Double offset = Math.abs(perfect_angle - user_angle);

        if(offset < maxOffset){
            responseType = "objective_cleared";
            finishObjective(headerAccessor);
            responseTrue = true;
        }
        else {
            responseType = "objective_cleared";
            responseTrue = false;
        }


        return new ServerMessage(responseType, responseTrue);
    }

    Boolean finishObjective(SimpMessageHeaderAccessor headerAccessor){
        Objective objective = (Objective) headerAccessor.getSessionAttributes().get("objective");

        Integer userId = (Integer) headerAccessor.getSessionAttributes().get("user_id");

        Integer missionId = objective.getMission_id();

        progressMissionsRepo.incrementObjectiveProgress(userId, missionId);

        return null;
    }


    /*
    private MessageHeaders createHeaders(String sessionId) {
        SimpMessageHeaderAccessor headerAccessor = SimpMessageHeaderAccessor.create(SimpMessageType.MESSAGE);
        headerAccessor.setSessionId(sessionId);
        headerAccessor.setLeaveMutable(true);
        return headerAccessor.getMessageHeaders();
    }
    */


    @MessageMapping("/init")
    @SendToUser("/topic/activity")
    public void init( @Payload ObjectiveInitRequest objectiveInitRequest, SimpMessageHeaderAccessor headerAccessor) throws Exception {
        Objective objective = objectiveRepo.getObjective(objectiveInitRequest.getObjective_id());
        headerAccessor.getSessionAttributes().put("user_id", objectiveInitRequest.getUser_id());
        headerAccessor.getSessionAttributes().put("objective", objective);
    }
}