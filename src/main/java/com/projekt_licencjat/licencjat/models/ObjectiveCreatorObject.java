package com.projekt_licencjat.licencjat.models;


import org.hibernate.annotations.Type;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public class ObjectiveCreatorObject {
    String description;

    String type;

    //@Type(type = "com.projekt_licencjat.licencjat.models.GenericArrayType")
    Double[] coordinates;

   // @Type(type = "com.projekt_licencjat.licencjat.models.GenericArrayType")
    Double[][] poly_points;

    Integer objective_number;

    String activity;

    public ObjectiveCreatorObject() {
    }

    public ObjectiveCreatorObject(String description, String type, Double[] coordinates,
                                  Double[][] poly_points, Integer objective_number, String activity) {
        this.description = description;
        this.type = type;
        this.coordinates = coordinates;
        this.poly_points = poly_points;
        this.objective_number = objective_number;
        this.activity = activity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObjectiveCreatorObject that = (ObjectiveCreatorObject) o;
        return Objects.equals(description, that.description) &&
                Objects.equals(type, that.type) &&
                Arrays.equals(coordinates, that.coordinates) &&
                Arrays.equals(poly_points, that.poly_points) &&
                Objects.equals(objective_number, that.objective_number) &&
                Objects.equals(activity, that.activity);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(description, type, objective_number, activity);
        result = 31 * result + Arrays.hashCode(coordinates);
        result = 31 * result + Arrays.hashCode(poly_points);
        return result;
    }

    @Override
    public String toString() {
        return "ObjectiveCreatorObject{" +
                "description='" + description + '\'' +
                ", type='" + type + '\'' +
                ", coordinates=" + Arrays.toString(coordinates) +
                ", poly_points=" + Arrays.toString(poly_points) +
                ", objective_number=" + objective_number +
                ", activity='" + activity + '\'' +
                '}';
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double[] getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Double[] coordinates) {
        this.coordinates = coordinates;
    }

    public Double[][] getPoly_points() {
        return poly_points;
    }

    public void setPoly_points(Double[][] poly_points) {
        this.poly_points = poly_points;
    }

    public Integer getObjective_number() {
        return objective_number;
    }

    public void setObjective_number(Integer objective_number) {
        this.objective_number = objective_number;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }
}

