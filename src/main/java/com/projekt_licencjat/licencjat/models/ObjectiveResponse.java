package com.projekt_licencjat.licencjat.models;

import java.util.Objects;

public class ObjectiveResponse {
    Boolean success;

    String responseText;

    Objective objective;

    public ObjectiveResponse() {
    }

    public ObjectiveResponse(Boolean success, String responseText, Objective objective) {
        this.success = success;
        this.responseText = responseText;
        this.objective = objective;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObjectiveResponse that = (ObjectiveResponse) o;
        return Objects.equals(success, that.success) &&
                Objects.equals(responseText, that.responseText) &&
                Objects.equals(objective, that.objective);
    }

    @Override
    public int hashCode() {
        return Objects.hash(success, responseText, objective);
    }

    @Override
    public String toString() {
        return "ObjectiveResponse{" +
                "sucdess=" + success +
                ", responseText='" + responseText + '\'' +
                ", objective=" + objective +
                '}';
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public Objective getObjective() {
        return objective;
    }

    public void setObjective(Objective objective) {
        this.objective = objective;
    }
}
