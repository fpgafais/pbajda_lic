package com.projekt_licencjat.licencjat.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import java.util.Objects;

@Entity
public class MissionResponse {
    @Id
    Integer mission_id;

    @OneToOne
    @PrimaryKeyJoinColumn(name = "mission_id")
    Mission mission;

    String status;


    public MissionResponse() {
    }

    public MissionResponse(Integer mission_id, Mission mission, String status) {
        this.mission_id = mission_id;
        this.mission = mission;
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MissionResponse that = (MissionResponse) o;
        return Objects.equals(mission_id, that.mission_id) &&
                Objects.equals(mission, that.mission) &&
                Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mission_id, mission, status);
    }

    @Override
    public String toString() {
        return "MissionResponse{" +
                "mission_id=" + mission_id +
                ", mission=" + mission +
                ", status='" + status + '\'' +
                '}';
    }

    public Integer getMission_id() {
        return mission_id;
    }

    public void setMission_id(Integer mission_id) {
        this.mission_id = mission_id;
    }

    public Mission getMission() {
        return mission;
    }

    public void setMission(Mission mission) {
        this.mission = mission;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

