package com.projekt_licencjat.licencjat.models;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Entity
public class Objective {
    @Id
    Integer objective_id;

    String description;

    Integer mission_id;

    String type;

    @Type(type = "com.projekt_licencjat.licencjat.models.GenericArrayType")
    Double[] coordinates;

    @Type(type = "com.projekt_licencjat.licencjat.models.GenericArrayType")
    Double[][] poly_points;

    Integer objective_number;

    String objective_json;

    public Objective() {
    }

    public Objective(Integer objective_id, String description, Integer mission_id, String type,
                     Double[] coordinates, Double[][] poly_points, Integer objective_number, String objective_json) {
        this.objective_id = objective_id;
        this.description = description;
        this.mission_id = mission_id;
        this.type = type;
        this.coordinates = coordinates;
        this.poly_points = poly_points;
        this.objective_number = objective_number;
        this.objective_json = objective_json;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Objective objective = (Objective) o;
        return Objects.equals(objective_id, objective.objective_id) &&
                Objects.equals(description, objective.description) &&
                Objects.equals(mission_id, objective.mission_id) &&
                Objects.equals(type, objective.type) &&
                Arrays.equals(coordinates, objective.coordinates) &&
                Arrays.equals(poly_points, objective.poly_points) &&
                Objects.equals(objective_number, objective.objective_number) &&
                Objects.equals(objective_json, objective.objective_json);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(objective_id, description, mission_id, type, objective_number, objective_json);
        result = 31 * result + Arrays.hashCode(coordinates);
        result = 31 * result + Arrays.hashCode(poly_points);
        return result;
    }

    @Override
    public String toString() {
        return "Objective{" +
                "objective_id=" + objective_id +
                ", description='" + description + '\'' +
                ", mission_id=" + mission_id +
                ", type='" + type + '\'' +
                ", coordinates=" + Arrays.toString(coordinates) +
                ", poly_points=" + Arrays.toString(poly_points) +
                ", objective_number=" + objective_number +
                ", objective_json='" + objective_json + '\'' +
                '}';
    }

    public Integer getObjective_id() {
        return objective_id;
    }

    public void setObjective_id(Integer objective_id) {
        this.objective_id = objective_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getMission_id() {
        return mission_id;
    }

    public void setMission_id(Integer mission_id) {
        this.mission_id = mission_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double[] getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Double[] coordinates) {
        this.coordinates = coordinates;
    }

    public Double[][] getPoly_points() {
        return poly_points;
    }

    public void setPoly_points(Double[][] poly_points) {
        this.poly_points = poly_points;
    }

    public Integer getObjective_number() {
        return objective_number;
    }

    public void setObjective_number(Integer objective_number) {
        this.objective_number = objective_number;
    }

    public String getObjective_json() {
        return objective_json;
    }

    public void setObjective_json(String objective_json) {
        this.objective_json = objective_json;
    }
}
