package com.projekt_licencjat.licencjat.models;

import java.util.Objects;

public class ServerMessage {
    String type;

    Boolean is_true;

    public ServerMessage() {
    }

    public ServerMessage(String type, Boolean is_true) {
        this.type = type;
        this.is_true = is_true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ServerMessage that = (ServerMessage) o;
        return Objects.equals(type, that.type) &&
                Objects.equals(is_true, that.is_true);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, is_true);
    }

    @Override
    public String toString() {
        return "ServerMessage{" +
                "type='" + type + '\'' +
                ", is_true=" + is_true +
                '}';
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getIs_true() {
        return is_true;
    }

    public void setIs_true(Boolean is_true) {
        this.is_true = is_true;
    }
}
