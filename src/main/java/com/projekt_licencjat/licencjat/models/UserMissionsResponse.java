package com.projekt_licencjat.licencjat.models;

import java.util.List;
import java.util.Objects;

public class UserMissionsResponse {
    List<UserMission> in_progress;
    List<UserMission> avaible;
    List<UserMission> finished;

    public UserMissionsResponse() {
    }

    public UserMissionsResponse(List<UserMission> in_progress, List<UserMission> avaible, List<UserMission> finished) {
        this.in_progress = in_progress;
        this.avaible = avaible;
        this.finished = finished;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserMissionsResponse that = (UserMissionsResponse) o;
        return Objects.equals(in_progress, that.in_progress) &&
                Objects.equals(avaible, that.avaible) &&
                Objects.equals(finished, that.finished);
    }

    @Override
    public int hashCode() {
        return Objects.hash(in_progress, avaible, finished);
    }

    @Override
    public String toString() {
        return "UserMissionsResponse{" +
                "in_progress=" + in_progress +
                ", avaible=" + avaible +
                ", finished=" + finished +
                '}';
    }

    public List<UserMission> getIn_progress() {
        return in_progress;
    }

    public void setIn_progress(List<UserMission> in_progress) {
        this.in_progress = in_progress;
    }

    public List<UserMission> getAvaible() {
        return avaible;
    }

    public void setAvaible(List<UserMission> avaible) {
        this.avaible = avaible;
    }

    public List<UserMission> getFinished() {
        return finished;
    }

    public void setFinished(List<UserMission> finished) {
        this.finished = finished;
    }
}
