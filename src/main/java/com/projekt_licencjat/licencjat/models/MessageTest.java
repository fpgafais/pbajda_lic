package com.projekt_licencjat.licencjat.models;

import java.util.Objects;

public class MessageTest {///DELETE THIS
    String author;

    String message;

    Ranking ranking;

    public MessageTest() {
    }

    public MessageTest(String author, String message, Ranking ranking) {
        this.author = author;
        this.message = message;
        this.ranking = ranking;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageTest that = (MessageTest) o;
        return Objects.equals(author, that.author) &&
                Objects.equals(message, that.message) &&
                Objects.equals(ranking, that.ranking);
    }

    @Override
    public int hashCode() {
        return Objects.hash(author, message, ranking);
    }

    @Override
    public String toString() {
        return "MessageTest{" +
                "author='" + author + '\'' +
                ", message='" + message + '\'' +
                ", ranking=" + ranking +
                '}';
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Ranking getRanking() {
        return ranking;
    }

    public void setRanking(Ranking ranking) {
        this.ranking = ranking;
    }
}
