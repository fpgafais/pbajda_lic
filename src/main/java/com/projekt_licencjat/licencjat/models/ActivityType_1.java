package com.projekt_licencjat.licencjat.models;

import java.util.Objects;

public class ActivityType_1 {
    Integer length;

    public ActivityType_1() {
    }

    public ActivityType_1(Integer length) {
        this.length = length;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActivityType_1 that = (ActivityType_1) o;
        return Objects.equals(length, that.length);
    }

    @Override
    public int hashCode() {
        return Objects.hash(length);
    }

    @Override
    public String toString() {
        return "ActivityType_1{" +
                "Length=" + length +
                '}';
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }


}
