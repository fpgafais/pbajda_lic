package com.projekt_licencjat.licencjat.models;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "ranking")
public class Ranking {
    @Id
    Integer ranking_id;

    Integer user_id;

    Integer points;

    Integer season_id;

    public Ranking(Integer ranking_id, Integer user_id, Integer points, Integer season_id) {
        this.ranking_id = ranking_id;
        this.user_id = user_id;
        this.points = points;
        this.season_id = season_id;
    }

    public Ranking() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ranking ranking = (Ranking) o;
        return Objects.equals(ranking_id, ranking.ranking_id) &&
                Objects.equals(user_id, ranking.user_id) &&
                Objects.equals(points, ranking.points) &&
                Objects.equals(season_id, ranking.season_id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ranking_id, user_id, points, season_id);
    }

    @Override
    public String toString() {
        return "Ranking{" +
                "ranking_id=" + ranking_id +
                ", user_id=" + user_id +
                ", points=" + points +
                ", season_id=" + season_id +
                '}';
    }

    public Integer getRanking_id() {
        return ranking_id;
    }

    public void setRanking_id(Integer ranking_id) {
        this.ranking_id = ranking_id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Integer getSeason_id() {
        return season_id;
    }

    public void setSeason_id(Integer season_id) {
        this.season_id = season_id;
    }
}
