package com.projekt_licencjat.licencjat.models;


import java.util.Objects;

public class RegistrationErrors {
    Boolean emailTaken;

    Boolean invalidEmail;

    Boolean passwordsDoNotMatch;

    Boolean emptyField;

    public RegistrationErrors() {
    }

    public RegistrationErrors(Boolean emailTaken, Boolean invalidEmail, Boolean passwordsDoNotMatch, Boolean emptyField) {
        this.emailTaken = emailTaken;
        this.invalidEmail = invalidEmail;
        this.passwordsDoNotMatch = passwordsDoNotMatch;
        this.emptyField = emptyField;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegistrationErrors that = (RegistrationErrors) o;
        return Objects.equals(emailTaken, that.emailTaken) &&
                Objects.equals(invalidEmail, that.invalidEmail) &&
                Objects.equals(passwordsDoNotMatch, that.passwordsDoNotMatch) &&
                Objects.equals(emptyField, that.emptyField);
    }

    @Override
    public int hashCode() {
        return Objects.hash(emailTaken, invalidEmail, passwordsDoNotMatch, emptyField);
    }

    @Override
    public String toString() {
        return "RegistrationErrors{" +
                "emailTaken=" + emailTaken +
                ", invalidEmail=" + invalidEmail +
                ", passwordsDoNotMatch=" + passwordsDoNotMatch +
                ", emptyField=" + emptyField +
                '}';
    }

    public Boolean getEmailTaken() {
        return emailTaken;
    }

    public void setEmailTaken(Boolean emailTaken) {
        this.emailTaken = emailTaken;
    }

    public Boolean getInvalidEmail() {
        return invalidEmail;
    }

    public void setInvalidEmail(Boolean invalidEmail) {
        this.invalidEmail = invalidEmail;
    }

    public Boolean getPasswordsDoNotMatch() {
        return passwordsDoNotMatch;
    }

    public void setPasswordsDoNotMatch(Boolean passwordsDoNotMatch) {
        this.passwordsDoNotMatch = passwordsDoNotMatch;
    }

    public Boolean getEmptyField() {
        return emptyField;
    }

    public void setEmptyField(Boolean emptyField) {
        this.emptyField = emptyField;
    }
}
