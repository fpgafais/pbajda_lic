package com.projekt_licencjat.licencjat.models;

import java.util.Objects;

public class UserActivityMessage_2 {
    Double longitude;

    Double latitude;

    Integer compass;

    public UserActivityMessage_2() {
    }

    public UserActivityMessage_2(Double longitude, Double latitude, Integer compass) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.compass = compass;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserActivityMessage_2 that = (UserActivityMessage_2) o;
        return Objects.equals(longitude, that.longitude) &&
                Objects.equals(latitude, that.latitude) &&
                Objects.equals(compass, that.compass);
    }

    @Override
    public int hashCode() {
        return Objects.hash(longitude, latitude, compass);
    }

    @Override
    public String toString() {
        return "UserActivityMessage_2{" +
                "longitude=" + longitude +
                ", latitude=" + latitude +
                ", compass=" + compass +
                '}';
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Integer getCompass() {
        return compass;
    }

    public void setCompass(Integer compass) {
        this.compass = compass;
    }
}
