package com.projekt_licencjat.licencjat.models;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

public class MissionCreatorObject {
    @NotEmpty
    @NotNull
    String name;

    @NotEmpty
    @NotNull
    String description;

    @NotEmpty
    @NotNull
    Integer minimumLvl;

    @NotEmpty
    @NotNull
    Integer missionDifficulty;

    @NotEmpty
    @NotNull
    ArrayList<ObjectiveCreatorObject> objectives;

    public MissionCreatorObject() {
    }

    public MissionCreatorObject(String name, String description, Integer minimumLvl,
                                Integer missionDifficulty, ArrayList<ObjectiveCreatorObject> objectives) {
        this.name = name;
        this.description = description;
        this.minimumLvl = minimumLvl;
        this.missionDifficulty = missionDifficulty;
        this.objectives = objectives;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MissionCreatorObject that = (MissionCreatorObject) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(minimumLvl, that.minimumLvl) &&
                Objects.equals(missionDifficulty, that.missionDifficulty) &&
                Objects.equals(objectives, that.objectives);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, minimumLvl, missionDifficulty, objectives);
    }

    @Override
    public String toString() {
        return "MissionCreatorObject{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", minimumLvl=" + minimumLvl +
                ", missionDifficulty=" + missionDifficulty +
                ", objectives=" + objectives +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getMinimumLvl() {
        return minimumLvl;
    }

    public void setMinimumLvl(Integer minimumLvl) {
        this.minimumLvl = minimumLvl;
    }

    public Integer getMissionDifficulty() {
        return missionDifficulty;
    }

    public void setMissionDifficulty(Integer missionDifficulty) {
        this.missionDifficulty = missionDifficulty;
    }

    public ArrayList<ObjectiveCreatorObject> getObjectives() {
        return objectives;
    }

    public void setObjectives(ArrayList<ObjectiveCreatorObject> objectives) {
        this.objectives = objectives;
    }
}
