package com.projekt_licencjat.licencjat.models;

import org.hibernate.annotations.Type;

import java.util.Arrays;

public class ActivityType_2 {
    @Type(type = "com.projekt_licencjat.licencjat.models.GenericArrayType")
    Double[] object_coordinates;

    public ActivityType_2() {
    }

    public ActivityType_2(Double[] object_coordinates) {
        this.object_coordinates = object_coordinates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActivityType_2 that = (ActivityType_2) o;
        return Arrays.equals(object_coordinates, that.object_coordinates);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(object_coordinates);
    }

    @Override
    public String toString() {
        return "ActivityType_2{" +
                "object_coordinates=" + Arrays.toString(object_coordinates) +
                '}';
    }

    public Double[] getObject_coordinates() {
        return object_coordinates;
    }

    public void setObject_coordinates(Double[] object_coordinates) {
        this.object_coordinates = object_coordinates;
    }
}
