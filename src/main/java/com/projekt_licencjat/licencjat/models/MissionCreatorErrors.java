package com.projekt_licencjat.licencjat.models;

import java.util.ArrayList;
import java.util.Objects;

public class MissionCreatorErrors {
    Boolean emptyField;

    Boolean tooLongName;

    Boolean tooLongDescription;

    Boolean wrongLvl;

    Boolean wrongDifficulty;

    Boolean imageTooLarge;

    Boolean objectiveError;


    public MissionCreatorErrors() {
    }

    public MissionCreatorErrors(Boolean emptyField, Boolean tooLongName, Boolean tooLongDescription,
                                Boolean wrongLvl, Boolean wrongDifficulty, Boolean imageTooLarge, Boolean objectiveError) {
        this.emptyField = emptyField;
        this.tooLongName = tooLongName;
        this.tooLongDescription = tooLongDescription;
        this.wrongLvl = wrongLvl;
        this.wrongDifficulty = wrongDifficulty;
        this.imageTooLarge = imageTooLarge;
        this.objectiveError = objectiveError;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MissionCreatorErrors that = (MissionCreatorErrors) o;
        return Objects.equals(emptyField, that.emptyField) &&
                Objects.equals(tooLongName, that.tooLongName) &&
                Objects.equals(tooLongDescription, that.tooLongDescription) &&
                Objects.equals(wrongLvl, that.wrongLvl) &&
                Objects.equals(wrongDifficulty, that.wrongDifficulty) &&
                Objects.equals(imageTooLarge, that.imageTooLarge) &&
                Objects.equals(objectiveError, that.objectiveError);
    }

    @Override
    public int hashCode() {
        return Objects.hash(emptyField, tooLongName, tooLongDescription, wrongLvl, wrongDifficulty, imageTooLarge, objectiveError);
    }

    @Override
    public String toString() {
        return "MissionCreatorErrors{" +
                "emptyField=" + emptyField +
                ", tooLongName=" + tooLongName +
                ", tooLongDescription=" + tooLongDescription +
                ", wrongLvl=" + wrongLvl +
                ", wrongDifficulty=" + wrongDifficulty +
                ", imageTooLarge=" + imageTooLarge +
                ", objectiveError=" + objectiveError +
                '}';
    }

    public Boolean getEmptyField() {
        return emptyField;
    }

    public void setEmptyField(Boolean emptyField) {
        this.emptyField = emptyField;
    }

    public Boolean getTooLongName() {
        return tooLongName;
    }

    public void setTooLongName(Boolean tooLongName) {
        this.tooLongName = tooLongName;
    }

    public Boolean getTooLongDescription() {
        return tooLongDescription;
    }

    public void setTooLongDescription(Boolean tooLongDescription) {
        this.tooLongDescription = tooLongDescription;
    }

    public Boolean getWrongLvl() {
        return wrongLvl;
    }

    public void setWrongLvl(Boolean wrongLvl) {
        this.wrongLvl = wrongLvl;
    }

    public Boolean getWrongDifficulty() {
        return wrongDifficulty;
    }

    public void setWrongDifficulty(Boolean wrongDifficulty) {
        this.wrongDifficulty = wrongDifficulty;
    }

    public Boolean getImageTooLarge() {
        return imageTooLarge;
    }

    public void setImageTooLarge(Boolean imageTooLarge) {
        this.imageTooLarge = imageTooLarge;
    }

    public Boolean getObjectiveError() {
        return objectiveError;
    }

    public void setObjectiveError(Boolean objectiveError) {
        this.objectiveError = objectiveError;
    }
}

