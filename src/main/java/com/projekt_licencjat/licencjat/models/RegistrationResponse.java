package com.projekt_licencjat.licencjat.models;

import java.util.Objects;

public class RegistrationResponse {
    //returns {success: bool, errors: {emailTaken: bool, validEmail: bool, passwordsDoNotMatch: bool}}
    Boolean success;

    RegistrationErrors errors;

    public RegistrationResponse() {
    }

    public RegistrationResponse(Boolean success, RegistrationErrors errors) {
        this.success = success;
        this.errors = errors;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegistrationResponse that = (RegistrationResponse) o;
        return Objects.equals(success, that.success) &&
                Objects.equals(errors, that.errors);
    }

    @Override
    public int hashCode() {
        return Objects.hash(success, errors);
    }

    @Override
    public String toString() {
        return "RegistrationResponse{" +
                "success=" + success +
                ", errors=" + errors +
                '}';
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public RegistrationErrors getErrors() {
        return errors;
    }

    public void setErrors(RegistrationErrors errors) {
        this.errors = errors;
    }
}

