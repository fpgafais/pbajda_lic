package com.projekt_licencjat.licencjat.models;

import java.util.Objects;

public class UserActivityMessage_1 {
    Long start;
    Long end;

    public UserActivityMessage_1() {
    }

    public UserActivityMessage_1(Long start, Long end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserActivityMessage_1 that = (UserActivityMessage_1) o;
        return Objects.equals(start, that.start) &&
                Objects.equals(end, that.end);
    }

    @Override
    public int hashCode() {
        return Objects.hash(start, end);
    }

    @Override
    public String toString() {
        return "UserActivityMessage_1{" +
                "start=" + start +
                ", end=" + end +
                '}';
    }

    public Long getStart() {
        return start;
    }

    public void setStart(Long start) {
        this.start = start;
    }

    public Long getEnd() {
        return end;
    }

    public void setEnd(Long end) {
        this.end = end;
    }
}


