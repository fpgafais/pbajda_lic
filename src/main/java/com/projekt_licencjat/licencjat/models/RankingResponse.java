package com.projekt_licencjat.licencjat.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class RankingResponse {
    List<RankingUser> ranking;

    RankingUser user;

    public RankingResponse() {
    }

    public RankingResponse(List<RankingUser> ranking, RankingUser user) {
        this.ranking = ranking;
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RankingResponse that = (RankingResponse) o;
        return Objects.equals(ranking, that.ranking) &&
                Objects.equals(user, that.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ranking, user);
    }

    @Override
    public String toString() {
        return "RankingResponse{" +
                "ranking=" + ranking +
                ", user=" + user +
                '}';
    }

    public List<RankingUser> getRanking() {
        return ranking;
    }

    public void setRanking(List<RankingUser> ranking) {
        this.ranking = ranking;
    }

    public RankingUser getUser() {
        return user;
    }

    public void setUser(RankingUser user) {
        this.user = user;
    }
}

