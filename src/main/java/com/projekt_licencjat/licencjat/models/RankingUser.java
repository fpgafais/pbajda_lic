package com.projekt_licencjat.licencjat.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class RankingUser {
    @Id
    Integer position;
    String username;
    Integer points;

    public RankingUser() {
    }

    public RankingUser(Integer position, String username, Integer points) {
        this.position = position;
        this.username = username;
        this.points = points;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RankingUser that = (RankingUser) o;
        return Objects.equals(position, that.position) &&
                Objects.equals(username, that.username) &&
                Objects.equals(points, that.points);
    }

    @Override
    public int hashCode() {
        return Objects.hash(position, username, points);
    }

    @Override
    public String toString() {
        return "RankingUser{" +
                "position=" + position +
                ", username='" + username + '\'' +
                ", points=" + points +
                '}';
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }
}
