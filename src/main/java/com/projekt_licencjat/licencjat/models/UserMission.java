package com.projekt_licencjat.licencjat.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class UserMission {
    @Id
    Integer mission_id;

    @OneToOne
    @PrimaryKeyJoinColumn(name = "mission_id")
    Mission mission;

    Integer objective_progress;

    public UserMission() {
    }

    public UserMission(Integer mission_id, Mission mission, Integer objective_progress) {
        this.mission_id = mission_id;
        this.mission = mission;
        this.objective_progress = objective_progress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserMission that = (UserMission) o;
        return Objects.equals(mission_id, that.mission_id) &&
                Objects.equals(mission, that.mission) &&
                Objects.equals(objective_progress, that.objective_progress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mission_id, mission, objective_progress);
    }

    @Override
    public String toString() {
        return "UserMission{" +
                "mission_id=" + mission_id +
                ", mission=" + mission +
                ", objective_progress=" + objective_progress +
                '}';
    }

    public Integer getMission_id() {
        return mission_id;
    }

    public void setMission_id(Integer mission_id) {
        this.mission_id = mission_id;
    }

    public Mission getMission() {
        return mission;
    }

    public void setMission(Mission mission) {
        this.mission = mission;
    }

    public Integer getObjective_progress() {
        return objective_progress;
    }

    public void setObjective_progress(Integer objective_progress) {
        this.objective_progress = objective_progress;
    }
}


