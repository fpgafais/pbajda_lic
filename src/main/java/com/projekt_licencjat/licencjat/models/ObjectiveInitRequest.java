package com.projekt_licencjat.licencjat.models;

import java.util.Objects;

public class ObjectiveInitRequest {
    Integer objective_id;

    Integer user_id;

    public ObjectiveInitRequest() {
    }

    public ObjectiveInitRequest(Integer objective_id, Integer user_id) {
        this.objective_id = objective_id;
        this.user_id = user_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObjectiveInitRequest that = (ObjectiveInitRequest) o;
        return Objects.equals(objective_id, that.objective_id) &&
                Objects.equals(user_id, that.user_id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(objective_id, user_id);
    }

    @Override
    public String toString() {
        return "ObjectiveInitRequest{" +
                "objective_id=" + objective_id +
                ", user_id=" + user_id +
                '}';
    }

    public Integer getObjective_id() {
        return objective_id;
    }

    public void setObjective_id(Integer objective_id) {
        this.objective_id = objective_id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }
}
