package com.projekt_licencjat.licencjat.models;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Objects;

@Entity
@Table(name="mission")
public class Mission {
    @Id
    Integer mission_id;

    String name;

    String description;

    Integer user_id;

    byte[] picture;

    Integer points;

    LocalDateTime creation_date;

    LocalDateTime modification_date;

    Boolean active;

    Integer objective_count;

    Integer difficulty_id;

    Integer minimum_points;


    public Mission() {
    }

    public Mission(Integer mission_id, String name, String description, Integer user_id, byte[] picture, Integer points,
                   LocalDateTime creation_date, LocalDateTime modification_date, Boolean active, Integer objective_count,
                   Integer difficulty_id, Integer minimum_points) {
        this.mission_id = mission_id;
        this.name = name;
        this.description = description;
        this.user_id = user_id;
        this.picture = picture;
        this.points = points;
        this.creation_date = creation_date;
        this.modification_date = modification_date;
        this.active = active;
        this.objective_count = objective_count;
        this.difficulty_id = difficulty_id;
        this.minimum_points = minimum_points;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Mission mission = (Mission) o;
        return Objects.equals(mission_id, mission.mission_id) &&
                Objects.equals(name, mission.name) &&
                Objects.equals(description, mission.description) &&
                Objects.equals(user_id, mission.user_id) &&
                Arrays.equals(picture, mission.picture) &&
                Objects.equals(points, mission.points) &&
                Objects.equals(creation_date, mission.creation_date) &&
                Objects.equals(modification_date, mission.modification_date) &&
                Objects.equals(active, mission.active) &&
                Objects.equals(objective_count, mission.objective_count) &&
                Objects.equals(difficulty_id, mission.difficulty_id) &&
                Objects.equals(minimum_points, mission.minimum_points);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mission_id, name, description, user_id, points, creation_date, modification_date,
                active, objective_count, difficulty_id, minimum_points);
        result = 31 * result + Arrays.hashCode(picture);
        return result;
    }

    @Override
    public String toString() {
        return "Mission{" +
                "mission_id=" + mission_id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", user_id=" + user_id +
                ", picture=" + Arrays.toString(picture) +
                ", points=" + points +
                ", creation_date=" + creation_date +
                ", modification_date=" + modification_date +
                ", active=" + active +
                ", objective_count=" + objective_count +
                ", difficulty_id=" + difficulty_id +
                ", minimum_points=" + minimum_points +
                '}';
    }

    public Integer getMission_id() {
        return mission_id;
    }

    public void setMission_id(Integer mission_id) {
        this.mission_id = mission_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public LocalDateTime getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(LocalDateTime creation_date) {
        this.creation_date = creation_date;
    }

    public LocalDateTime getModification_date() {
        return modification_date;
    }

    public void setModification_date(LocalDateTime modification_date) {
        this.modification_date = modification_date;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getObjective_count() {
        return objective_count;
    }

    public void setObjective_count(Integer objective_count) {
        this.objective_count = objective_count;
    }

    public Integer getDifficulty_id() {
        return difficulty_id;
    }

    public void setDifficulty_id(Integer difficulty_id) {
        this.difficulty_id = difficulty_id;
    }

    public Integer getMinimum_points() {
        return minimum_points;
    }

    public void setMinimum_points(Integer minimum_points) {
        this.minimum_points = minimum_points;
    }
}
