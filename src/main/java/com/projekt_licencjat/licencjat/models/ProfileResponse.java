package com.projekt_licencjat.licencjat.models;

import java.util.List;
import java.util.Objects;

public class ProfileResponse {
    User userData;
    Ranking ranking;
    Integer finishedMissions;

    public ProfileResponse() {
    }

    public ProfileResponse(User userData, Ranking ranking, Integer finishedMissions) {
        this.userData = userData;
        this.ranking = ranking;
        this.finishedMissions = finishedMissions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProfileResponse that = (ProfileResponse) o;
        return Objects.equals(userData, that.userData) &&
                Objects.equals(ranking, that.ranking) &&
                Objects.equals(finishedMissions, that.finishedMissions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userData, ranking, finishedMissions);
    }

    @Override
    public String toString() {
        return "ProfileResponse{" +
                "userData=" + userData +
                ", ranking=" + ranking +
                ", finishedMissions=" + finishedMissions +
                '}';
    }

    public User getUserData() {
        return userData;
    }

    public void setUserData(User userData) {
        this.userData = userData;
    }

    public Ranking getRanking() {
        return ranking;
    }

    public void setRanking(Ranking ranking) {
        this.ranking = ranking;
    }

    public Integer getFinishedMissions() {
        return finishedMissions;
    }

    public void setFinishedMissions(Integer finishedMissions) {
        this.finishedMissions = finishedMissions;
    }
}

