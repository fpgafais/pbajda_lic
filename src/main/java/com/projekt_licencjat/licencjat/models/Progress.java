package com.projekt_licencjat.licencjat.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name="progress")
public class Progress {
    @Id
    Integer progress_id;

    Integer user_id;

    Integer mission_id;

    Boolean finished;

    LocalDateTime started_date;

    LocalDateTime last_progress_date;

    Integer objective_progress;

    public Progress(Integer progress_id, Integer user_id, Integer mission_id, Boolean finished,
                    LocalDateTime started_date, LocalDateTime last_progress_date, Integer objective_progress) {
        this.progress_id = progress_id;
        this.user_id = user_id;
        this.mission_id = mission_id;
        this.finished = finished;
        this.started_date = started_date;
        this.last_progress_date = last_progress_date;
        this.objective_progress = objective_progress;
    }

    public Progress() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Progress progress = (Progress) o;
        return Objects.equals(progress_id, progress.progress_id) &&
                Objects.equals(user_id, progress.user_id) &&
                Objects.equals(mission_id, progress.mission_id) &&
                Objects.equals(finished, progress.finished) &&
                Objects.equals(started_date, progress.started_date) &&
                Objects.equals(last_progress_date, progress.last_progress_date) &&
                Objects.equals(objective_progress, progress.objective_progress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(progress_id, user_id, mission_id, finished, started_date, last_progress_date, objective_progress);
    }

    @Override
    public String toString() {
        return "Progress{" +
                "progress_id=" + progress_id +
                ", user_id=" + user_id +
                ", mission_id=" + mission_id +
                ", finished=" + finished +
                ", started_date=" + started_date +
                ", last_progress_date=" + last_progress_date +
                ", objective_progress=" + objective_progress +
                '}';
    }

    public Integer getProgress_id() {
        return progress_id;
    }

    public void setProgress_id(Integer progress_id) {
        this.progress_id = progress_id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getMission_id() {
        return mission_id;
    }

    public void setMission_id(Integer mission_id) {
        this.mission_id = mission_id;
    }

    public Boolean getFinished() {
        return finished;
    }

    public void setFinished(Boolean finished) {
        this.finished = finished;
    }

    public LocalDateTime getStarted_date() {
        return started_date;
    }

    public void setStarted_date(LocalDateTime started_date) {
        this.started_date = started_date;
    }

    public LocalDateTime getLast_progress_date() {
        return last_progress_date;
    }

    public void setLast_progress_date(LocalDateTime last_progress_date) {
        this.last_progress_date = last_progress_date;
    }

    public Integer getObjective_progress() {
        return objective_progress;
    }

    public void setObjective_progress(Integer objective_progress) {
        this.objective_progress = objective_progress;
    }
}
