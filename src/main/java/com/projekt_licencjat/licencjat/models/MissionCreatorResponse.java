package com.projekt_licencjat.licencjat.models;

import java.util.Objects;

public class MissionCreatorResponse {
    Boolean success;

    MissionCreatorErrors errors;

    Integer missionId;

    public MissionCreatorResponse() {
    }

    public MissionCreatorResponse(Boolean success, MissionCreatorErrors errors, Integer missionId) {
        this.success = success;
        this.errors = errors;
        this.missionId = missionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MissionCreatorResponse that = (MissionCreatorResponse) o;
        return Objects.equals(success, that.success) &&
                Objects.equals(errors, that.errors) &&
                Objects.equals(missionId, that.missionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(success, errors, missionId);
    }

    @Override
    public String toString() {
        return "MissionCreatorResponse{" +
                "success=" + success +
                ", errors=" + errors +
                ", missionId=" + missionId +
                '}';
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public MissionCreatorErrors getErrors() {
        return errors;
    }

    public void setErrors(MissionCreatorErrors errors) {
        this.errors = errors;
    }

    public Integer getMissionId() {
        return missionId;
    }

    public void setMissionId(Integer missionId) {
        this.missionId = missionId;
    }
}

