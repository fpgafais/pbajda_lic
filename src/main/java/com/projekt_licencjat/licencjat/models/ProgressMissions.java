package com.projekt_licencjat.licencjat.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
public class ProgressMissions {
    @Id
    Integer progress_missions_id;

    Integer user_id;

    Integer mission_id;

    String status;

    LocalDateTime status_change_date;

    Integer objective_progress;

    public ProgressMissions() {
    }

    public ProgressMissions(Integer progress_missions_id, Integer user_id, Integer mission_id,
                            String status, LocalDateTime status_change_date, Integer objective_progress) {
        this.progress_missions_id = progress_missions_id;
        this.user_id = user_id;
        this.mission_id = mission_id;
        this.status = status;
        this.status_change_date = status_change_date;
        this.objective_progress = objective_progress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProgressMissions that = (ProgressMissions) o;
        return Objects.equals(progress_missions_id, that.progress_missions_id) &&
                Objects.equals(user_id, that.user_id) &&
                Objects.equals(mission_id, that.mission_id) &&
                Objects.equals(status, that.status) &&
                Objects.equals(status_change_date, that.status_change_date) &&
                Objects.equals(objective_progress, that.objective_progress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(progress_missions_id, user_id, mission_id, status, status_change_date, objective_progress);
    }

    @Override
    public String toString() {
        return "ProgressMissions{" +
                "progress_missions_id=" + progress_missions_id +
                ", user_id=" + user_id +
                ", mission_id=" + mission_id +
                ", status='" + status + '\'' +
                ", status_change_date=" + status_change_date +
                ", objective_progress=" + objective_progress +
                '}';
    }

    public Integer getProgress_missions_id() {
        return progress_missions_id;
    }

    public void setProgress_missions_id(Integer progress_missions_id) {
        this.progress_missions_id = progress_missions_id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getMission_id() {
        return mission_id;
    }

    public void setMission_id(Integer mission_id) {
        this.mission_id = mission_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getStatus_change_date() {
        return status_change_date;
    }

    public void setStatus_change_date(LocalDateTime status_change_date) {
        this.status_change_date = status_change_date;
    }

    public Integer getObjective_progress() {
        return objective_progress;
    }

    public void setObjective_progress(Integer objective_progress) {
        this.objective_progress = objective_progress;
    }
}
