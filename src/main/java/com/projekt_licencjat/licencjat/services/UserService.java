package com.projekt_licencjat.licencjat.services;

import com.projekt_licencjat.licencjat.models.User;
import com.projekt_licencjat.licencjat.models.UserRegistration;
import com.projekt_licencjat.licencjat.repositories.UserRepository;
import com.projekt_licencjat.licencjat.validation.EmailExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class UserService implements IUserService{
    @Autowired
    private UserRepository repository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Transactional
    public User registerNewUserAccount(UserRegistration accountRegistration) throws EmailExistsException {

        if (emailExist(accountRegistration.getEmail())) {
            throw new EmailExistsException(
                    "There is an account with that email adress: "
                            + accountRegistration.getEmail());
        }

        User user = repository.registerUser(accountRegistration.getUsername(), accountRegistration.getEmail(),
                passwordEncoder.encode(accountRegistration.getPassword()),"ROLE_USER");

        return user;
    }

    private boolean emailExist(String email) {
        User user = repository.findByEmail(email);
        if (user != null) {
            return true;
        }
        return false;
    }
}
