package com.projekt_licencjat.licencjat.services;

import com.projekt_licencjat.licencjat.models.User;
import com.projekt_licencjat.licencjat.models.UserRegistration;
import com.projekt_licencjat.licencjat.validation.EmailExistsException;

public interface IUserService {
    User registerNewUserAccount(UserRegistration accountDto)
            throws EmailExistsException;
}
