package com.projekt_licencjat.licencjat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.CrossOrigin;

@SpringBootApplication
@EnableJpaRepositories
public class LicencjatApplication {

	public static void main(String[] args) {
		SpringApplication.run(LicencjatApplication.class, args);
	}

}
