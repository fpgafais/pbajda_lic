package com.projekt_licencjat.licencjat.repositories;

import com.projekt_licencjat.licencjat.models.Mission;
import com.projekt_licencjat.licencjat.models.UserMission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserMissionRepository extends JpaRepository<UserMission, String> {
    @Query(value = "SELECT pm.mission_id, m as mission, pm.objective_progress FROM progress_missions pm JOIN mission m ON (pm.mission_id = m.mission_id) " +
            "WHERE pm.user_id = ?1 AND pm.status = ?2 ",
            nativeQuery = true)
    List<UserMission> getUserMissions(Integer user_id, String status);//status: active/finished/on-hold/replay

    @Query(value = "SELECT m.mission_id, m as mission , 0 as objective_progress FROM mission as m left outer join ( " +
            "SELECT * FROM progress_missions WHERE user_id = ?1 ) as pm on " +
            "m.mission_id = pm.mission_id where pm.mission_id IS NULL AND ?2 >= m.minimum_points AND m.active = ?3 ",
            nativeQuery = true)
    List<UserMission> getUserAvaibleMissions(Integer userId, Integer user_points, Boolean active);
}
