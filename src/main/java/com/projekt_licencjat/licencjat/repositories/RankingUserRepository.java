package com.projekt_licencjat.licencjat.repositories;

import com.projekt_licencjat.licencjat.models.MissionResponse;
import com.projekt_licencjat.licencjat.models.RankingUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RankingUserRepository extends JpaRepository<RankingUser, String> {
    @Query(value = "SELECT row_number() OVER (ORDER BY r.points DESC) as position , u.username, r.points " +
            " FROM ranking as r JOIN users as u ON u.user_id = r.user_id LIMIT 10 ",
            nativeQuery = true)
    List<RankingUser> getRanking();

    @Query(value ="SELECT n.position, n.username, n.points FROM (SELECT row_number() OVER " +
            "(ORDER BY r.points DESC) as position , u.username, r.points , u.user_id " +
            " FROM ranking as r JOIN users as u ON u.user_id = r.user_id) as n WHERE n.user_id = ?1 ",
            nativeQuery = true)
    RankingUser getUserRanking(Integer userId);
}
