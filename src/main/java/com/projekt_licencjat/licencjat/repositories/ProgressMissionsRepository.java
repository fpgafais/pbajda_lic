package com.projekt_licencjat.licencjat.repositories;

import com.projekt_licencjat.licencjat.models.ProgressMissions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface ProgressMissionsRepository extends JpaRepository<ProgressMissions, String> {
    @Query(value = "SELECT * FROM progress_missions WHERE mission_id = ?2 AND user_id = ?1",
            nativeQuery  = true)
    ProgressMissions getMissionsProgress(Integer userId, Integer missionId);

    @Query(value = "INSERT INTO public.progress_missions(user_id, mission_id) VALUES ( ?1, ?2) RETURNING *",
            nativeQuery = true)
    ProgressMissions addProgress(Integer userId, Integer missionId);

    @Query(value = "UPDATE public.progress_missions SET status='replay', objective_progress=1 WHERE progress_missions_id = ?1 RETURNING *",
            nativeQuery = true)
    ProgressMissions replayMission(Integer progressMissionsId);


    @Query(value = "UPDATE public.progress_missions SET status='finished', objective_progress=1 WHERE progress_missions_id = ?1 RETURNING *",
            nativeQuery = true)
    ProgressMissions finishMission(Integer progressMissionsId);

    @Query(value="UPDATE public.progress_missions" +
            " SET objective_progress=objective_progress+1" +
            " WHERE user_id=?1 AND mission_id = ?2 RETURNING *", nativeQuery = true)//test
    ProgressMissions incrementObjectiveProgress(Integer userId, Integer missionId);
}
