package com.projekt_licencjat.licencjat.repositories;

import com.projekt_licencjat.licencjat.models.MissionResponse;
import com.projekt_licencjat.licencjat.models.Objective;
import org.hibernate.annotations.Type;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.sql.Array;


public interface ObjectiveRepository extends JpaRepository<Objective, String> {

    @Query(value ="SELECT * FROM objective WHERE objective_id = ?1 ",
            nativeQuery = true)
    Objective getObjective(Integer objectiveId);


    @Query(value = "SELECT * FROM objective WHERE mission_id = ?1 AND objective_number = ?2",
            nativeQuery = true)
    Objective getCurrentObjective(Integer missionId, Integer mission_progress);

    @Query(value ="INSERT INTO public.objective(" +
            " description, mission_id, objective_number, type, coordinates, poly_points, objective_json) " +
            " VALUES ( ?1, ?2, ?3, ?4, ?5, ?6, ?7) RETURNING * ", nativeQuery =  true)
    Objective addObjective(String description, Integer mission_id, Integer objective_number, String type,
                           Double[] coordinates, Double[][] poly_points, String objective_json);

}
