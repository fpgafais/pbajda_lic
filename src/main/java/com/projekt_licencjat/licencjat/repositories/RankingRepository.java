package com.projekt_licencjat.licencjat.repositories;

import com.projekt_licencjat.licencjat.models.Ranking;
import com.projekt_licencjat.licencjat.models.RankingResponse;
import com.projekt_licencjat.licencjat.models.RankingUser;
import com.projekt_licencjat.licencjat.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RankingRepository  extends JpaRepository<Ranking, String> {

    @Query(value = "SELECT * FROM ranking WHERE season_id = ?1 ",
                nativeQuery = true)
    List<Ranking> getRankingBySeason(int seasonId);

    @Query(value = "SELECT * FROM ranking WHERE season_id = ?1 AND user_id = ?2",
            nativeQuery = true)
    Ranking getUserRankingBySeason(int seasonId, int userId);

    @Query(value ="UPDATE ranking SET points = points + ?2 WHERE user_id = ?1 RETURNING points",
            nativeQuery = true)
    Integer giveUserPoints(Integer userId, Integer points);

    @Query(value = "INSERT INTO ranking (user_id) VALUES ( ?1 ) RETURNING *",
            nativeQuery = true)
    Ranking addUser(Integer userId);

}
