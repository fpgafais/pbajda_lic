package com.projekt_licencjat.licencjat.repositories;

import com.projekt_licencjat.licencjat.models.ObjectiveCreatorObject;
import com.projekt_licencjat.licencjat.models.ObjectiveType;
import org.postgresql.jdbc.PgArray;
import org.postgresql.jdbc.PgConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ObjectiveRepoTransaction {
    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    JdbcTemplate jdbcTemplate;



    @Transactional
    public Boolean insertObjective( Integer mission_id, ArrayList<ObjectiveCreatorObject> objectives)
            throws SQLException {
        Boolean err = false;

        int index = 0;

        for(ObjectiveCreatorObject objective: objectives) {
             index++;
             Connection conn = jdbcTemplate.getDataSource().getConnection();
             PreparedStatement ps = conn.prepareStatement("INSERT INTO public.objective( " +
                     " description, mission_id, objective_number, type, coordinates, poly_points, objective_json) " +
                     " VALUES ( ?, ?, ?, ?, ?, ?, ?)");
             if(objective.getDescription().length() <= 300)
                ps.setString(1, objective.getDescription());
             else
                 err = true;

             ps.setInt(2, mission_id);
             ps.setInt(3, index);

             if(checkIfObjType(objective.getType()))
                ps.setString(4, objective.getType());
             else
                 err = true;

             if(objective.getCoordinates().length != 0)
                ps.setArray(5, createSqlArrayOne(objective.getCoordinates()));
             else
                 err = true;

             if(objective.getCoordinates().length != 0)
                ps.setArray(6, createSqlArrayDouble(objective.getPoly_points()));
             else
                 err = false;

             if(objective.getActivity().length() != 0)
                ps.setString(7, objective.getActivity());
             else
                 err = false;

             ps.executeUpdate();

            if(err){
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return false;
            }
         }

         return true;
    }

    private Boolean checkIfObjType(String str){
        for(ObjectiveType type: ObjectiveType.values()){
            if(type.name().equalsIgnoreCase(str)){
                return true;
            }
        }
        return false;
    }

    private PgArray createSqlArrayOne(Double[] list){
        System.out.println("first");
        PgArray intArray = null;
        try {
            /*
            Integer tab[] = {1,2};
            Connection conn = jdbcTemplate.getDataSource().getConnection();
            System.out.println(conn.getClientInfo());
            intArray = (PgArray) conn.createArrayOf("float8", list);
            System.out.println(intArray.getBaseTypeName());
            */
            Connection conn = jdbcTemplate.getDataSource().getConnection();
            System.out.println(conn);
            intArray = (PgArray) conn.createArrayOf("float8", list);

        } catch (SQLException ignore) {
            System.out.println("ERROR ARRAY");
        }

        System.out.println(intArray);
        System.out.println("first");
        return intArray;
    }

    private Array createSqlArrayDouble(Double[][] list){
        System.out.println("second");
        Array intArray = null;
        try {
            Integer tab[] = {1,2};
            Connection conn = jdbcTemplate.getDataSource().getConnection();
            System.out.println(conn.getClientInfo());
            intArray = conn.createArrayOf("float8", list);
        } catch (SQLException ignore) {
            System.out.println("ERROR ARRAY");
        }

        System.out.println(intArray);

        System.out.println("second");
        return intArray;
    }
}

