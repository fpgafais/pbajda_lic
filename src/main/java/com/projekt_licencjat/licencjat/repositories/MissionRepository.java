package com.projekt_licencjat.licencjat.repositories;

import com.projekt_licencjat.licencjat.models.Mission;
import com.projekt_licencjat.licencjat.models.MissionResponse;
import com.projekt_licencjat.licencjat.models.Ranking;
import com.projekt_licencjat.licencjat.models.UserMission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

public interface MissionRepository extends JpaRepository<MissionResponse, String> {
    @Query(value = "SELECT COUNT(*) FROM progress_missions WHERE status = 'finished' AND user_id = ?1 ",
            nativeQuery = true)
    Integer getCountFinishedMissions(Integer user_id);

    @Query(value = "SELECT m.*, pm.status FROM mission m LEFT OUTER JOIN " +
            "(SELECT * FROM progress_missions WHERE user_id = ?1) pm " +
            "ON (m.mission_id = pm.mission_id) WHERE m.mission_id = ?2", nativeQuery = true)
    MissionResponse getMission(Integer user_id, Integer mission_id);

    @Query(value = "SELECT minimum_points FROM mission WHERE mission_id = ?1", nativeQuery = true)
    Integer getMinimumPoints(Integer missionId);

    @Query(value = "SELECT COUNT(*) FROM progress_missions WHERE status = 'replay' AND user_id = ?1 ",
            nativeQuery =  true)
    Integer getCountReplayMissions(Integer user_id);

    @Query(value = "INSERT INTO public.mission(" +
            " name, description, user_id, picture, points, active, objective_count, difficulty_id, minimum_points)\n" +
            " VALUES ( ?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9) RETURNING mission_id ",
            nativeQuery =  true)
    Integer addMission(String name, String description, Integer userId, byte[] picture, Integer points,
                       Boolean active, Integer objectiveCount, Integer difficulty, Integer minimum_points);

    @Modifying
    @Query(value ="DELETE FROM public.mission WHERE mission_id = ?1 ",
            nativeQuery = true)
    Integer deleteMission(Integer missionId);
}
