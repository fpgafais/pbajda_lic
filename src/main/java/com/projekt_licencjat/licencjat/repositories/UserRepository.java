package com.projekt_licencjat.licencjat.repositories;

import com.projekt_licencjat.licencjat.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<User, String> {
    @Query(value = "INSERT INTO users(username, email, password, roles) VALUES (?1, ?2, ?3, ?4) RETURNING *", nativeQuery = true)
    User registerUser(String username, String email, String password, String role);


    @Query(value = "SELECT * FROM users WHERE email = ?1", nativeQuery = true)
    User findByEmail(String email);


    //@Query(value ="SELECT * FROM users WHERE username = ?1 AND password = ?2" , nativeQuery = true)
    //User findByUsernamePassword(String username, String password);
}
