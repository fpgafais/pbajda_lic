package com.projekt_licencjat.licencjat.validation;


@SuppressWarnings("serial")
public class EmailExistsException extends Throwable{
    public EmailExistsException(final String message) {
        super(message);
    }
}
