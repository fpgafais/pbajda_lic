package com.projekt_licencjat.licencjat.validation;

import com.projekt_licencjat.licencjat.models.UserRegistration;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {

    @Override
    public void initialize(PasswordMatches constraintAnnotation) {
    }
    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext context){
        UserRegistration user = (UserRegistration) obj;
        return user.getPassword().equals(user.getMatchingPassword());
    }
}
