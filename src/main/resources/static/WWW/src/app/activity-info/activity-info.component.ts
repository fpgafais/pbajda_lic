import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';

@Component({
  selector: 'app-activity-info',
  templateUrl: './activity-info.component.html',
  styleUrls: ['./activity-info.component.sass']
})
export class ActivityInfoComponent implements OnInit {
  activityType: String;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog
  ) { }
 

  ngOnInit() {
    this.activityType = this.data['activityType'];
  }


  close(){
    this.dialog.closeAll();
  }
}
