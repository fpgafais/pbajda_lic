import { Component, OnInit } from '@angular/core';
import { Registration } from '../model/registration';
import { HttpService } from '../http/http.service';
import { RegistrationResponse } from '../model/registration-response';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.sass']
})
export class RegistrationComponent implements OnInit {

  userDetails: Registration;
  response: RegistrationResponse;

  constructor(
    private http: HttpService,
    private router: Router,
  ) { 
    this.response = new RegistrationResponse();
    this.userDetails = new Registration();
    
  }

  ngOnInit() {

  }

  sendForm(){
    this.http.sendRegistrationForm(this.userDetails).subscribe((res : RegistrationResponse)=>{
      this.response = res;

      if(this.response.success)
        this.succesfullRegistration();
    })
  }

  succesfullRegistration(){
    setTimeout(() => {
      this.router.navigate(['/login']);
    }, 1000);
  }



}
