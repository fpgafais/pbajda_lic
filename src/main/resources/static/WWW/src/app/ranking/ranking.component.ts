import { Component, OnInit } from '@angular/core';
import { RankingUser } from '../model/ranking-user';
import { HttpService } from '../http/http.service';
import { RankingResponse } from '../model/ranking-response';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.sass']
})
export class RankingComponent implements OnInit {

  ranking: RankingUser[];
  user: RankingUser;

  loaded: boolean = false;

  constructor(
    private http: HttpService,
  ) { }

  ngOnInit() {
    this.http.getRanking().subscribe((response: RankingResponse) => {
      this.ranking = response.ranking;
      this.user = response.user;

      console.log(this.ranking.findIndex((r) =>{return r.position == this.user.position}));
    })
  }




}
