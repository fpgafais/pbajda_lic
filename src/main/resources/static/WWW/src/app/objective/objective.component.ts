import { Component, OnInit, OnDestroy, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { RxStompService } from '@stomp/ng2-stompjs';
import { AuthService } from '../authentication/auth.service';
import { GeoLocationService } from '../geolocation/geo-location.service';
import { circular } from 'ol/geom/Polygon';
import { Message } from '@stomp/stompjs';
import { Objective } from '../model/objective';
import { HttpService } from '../http/http.service';
import { ObjectiveResponse } from '../model/objective-response';
import { ActivityType1 } from '../model/activity-type1';
import { Activity } from '../model/activity';
import { ActivityType3 } from '../model/activity-type3';
import { ActivityType2 } from '../model/activity-type2';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material';
import { ActivityCameraComponent } from '../activity-camera/activity-camera.component';
import { myRxStompConfig } from '../my-rx-stomp.config';
import {ServerMessage} from '../model/server-message';
import { ObjectiveInfoComponent } from '../objective-info/objective-info.component';
import { ActivityInfoComponent } from '../activity-info/activity-info.component';
import { ActivityWaitComponent } from '../activity-wait/activity-wait.component';
import { resolve } from 'url';
import { ObjectiveFinishedComponent } from '../objective-finished/objective-finished.component';


@Component({
  selector: 'app-objective',
  templateUrl: './objective.component.html',
  styleUrls: ['./objective.component.sass'],
  host: {
    'class': 'router-flex'
  }
})
export class ObjectiveComponent implements OnInit, OnDestroy, AfterViewInit {

  state$:  Observable<object>;
  missionId: string;
  topicSubscription: any;
  locationsSubscription: any;
  objective: Objective;
  activity: any;
  insidePolygon = false;
  compass : number = 0;
  activityCoords;

  servMessage: ServerMessage = null;
  viewLoaded: boolean = false;

  infoButtonClicked = false;

  activityDialog: MatDialogRef<any>;
  commonDialog: MatDialogRef<any>;

  constructor(
    private route: ActivatedRoute,
    private rxStompService: RxStompService,//Web Sockets
    private auth: AuthService,
    private geoLocation : GeoLocationService,
    private cdRef : ChangeDetectorRef,//change Detection for correct map flex grow
    private http: HttpService,
    public dialog: MatDialog,
    public router: Router,
    ) {
      myRxStompConfig.connectHeaders['Authorization'] = this.auth.getToken();
      this.startConnection();
     }

  ngOnInit() {
    this.insidePolygon = false;
    this.compass = 0;
    this.servMessage = null;
    this.viewLoaded = false;
    this.infoButtonClicked = false;

    if(window.history.state['mission_id']){
      localStorage.setItem('mission_id', window.history.state['mission_id']);
    }


    this.missionId = localStorage.getItem('mission_id');

    this.getObjectiveInfo();
  }

  ngOnDestroy(){
    this.closeConnection();
  }

  ngAfterViewInit(){  
    //this.resetChildForm();
  }

  getObjectiveInfo(){
    this.http.getObjective(this.missionId).subscribe((res : ObjectiveResponse) => {
      if(res.success){
        this.objective = res.objective;
        switch(res.objective.type){
          case 'type_1': {
            this.activity = <ActivityType1>JSON.parse(res.objective['objective_json']);
          }
          case 'type_2': {
            this.activity = <ActivityType2>JSON.parse(res.objective['objective_json']);
            this.activityCoords = this.activity['object_coordinates'];
          }
          case 'type_3': {
            this.activity = <ActivityType3>JSON.parse(res.objective['objective_json']);
          }
        };
        this.socketInit();
        this.resetChildForm();
      }
      else{
        if(res.responseText == 'finished_mission'){
          //alert("finished");
          this.openObjectiveFinishDialog("the entire mission");
          this.router.navigate(['/missions']);
        }

      }
    })
  }

  intersectOutputFromChild($event){
      this.insidePolygon = $event;
      if(this.insidePolygon == false)
        if(this.activityDialog)
          this.activityDialog.close();
  }

  socketInit(){
    this.rxStompService.publish({destination: '/app/init', body: JSON.stringify({
      'objective_id': this.objective.objective_id,
      'user_id' : HttpService.user_id})});
  }

  objectiveOnFinish(){
    this.openObjectiveFinishDialog("objective " + this.objective.objective_number);
    this.ngOnInit();
    //window.location.reload();
  }


  compassOutputFromChild($event){
    this.compass = $event;
  }

  resetChildForm(){//fixes the flex box problem of child component
    this.viewLoaded = false;

    setTimeout(() => {
        this.viewLoaded = true
      }, 100);
  }


  startConnection(){
    this.topicSubscription = this.rxStompService.watch('/user/topic/activity').subscribe((message: Message) => {
      this.servMessage = JSON.parse(message.body);
      if(this.servMessage.type=="objective_cleared" && this.servMessage.is_true){
        this.objectiveOnFinish();
      }
    });

    this.locationsSubscription = GeoLocationService.locations.subscribe((position: Position)=>{
      const coords = [position.coords.longitude, position.coords.latitude];
      const accuracy = circular(coords, position.coords.accuracy);    
    });
  }

  closeConnection(){
    if(this.topicSubscription){
      this.topicSubscription.unsubscribe();
    if(this.locationsSubscription)
      this.locationsSubscription.unsubscribe();
    }
  }

  openObjectiveInfo(){
    this.infoButtonClicked = true;
    let config = new MatDialogConfig();
      config = {
        height: '70%',
        width: '99%',
        panelClass: 'activity-overlay',
        data: {
          objectiveInfo: this.objective.description,
        }
      };

    this.commonDialog = this.dialog.open(ObjectiveInfoComponent, config);

  }

  openActivityInfo(){
    this.infoButtonClicked = true;
    let config = new MatDialogConfig();
      config = {
        height: '70%',
        width: '99%',
        panelClass: 'activity-overlay',
        data: {
          activityType: this.objective.type,
        }
      };

    this.commonDialog = this.dialog.open(ActivityInfoComponent, config);

  }


  openActivity(){
      let config = new MatDialogConfig();
      config = {
        height: '70%',
        width: '99%',
        panelClass: 'activity-overlay'
      };

    switch(this.objective.type){
      case "type_1": {
        this.openWaitActivity(config);
        break;
      }
      case "type_2": {
        this.openCameraActivity(config);
        break;
      }


    }
    
  }

  openCameraActivity(config: MatDialogConfig){
    this.activityDialog = this.dialog.open(ActivityCameraComponent,config);

    this.activityDialog.afterClosed().subscribe((takenPhoto : Boolean) => {
        if(takenPhoto)
            this.sendUserCoords();
      })
  }

  openWaitActivity(config: MatDialogConfig){
    config.data = this.activity;
    this.activityDialog = this.dialog.open(ActivityWaitComponent,config);

    this.activityDialog.afterClosed().subscribe((response : {start: Number, end: Number}) => {
        if(response)
            this.sendUserWaitPoints(response);
      })
  }

  sendUserCoords(){
    //alert("TakenPhoto " + GeoLocationService.longitude + " "+ GeoLocationService.latitude + " " + this.compass);
    var userActivityMessage = {
      longitude: GeoLocationService.longitude,
      latitude: GeoLocationService.latitude,
      compass: this.compass
    }

    this.rxStompService.publish({destination: '/app/activity_2', body: JSON.stringify(userActivityMessage)});
  }

  sendUserWaitPoints(userActivityMessage: {start: Number, end: Number}){
    this.rxStompService.publish({destination: '/app/activity_1', body: JSON.stringify(userActivityMessage)});
  }


  openObjectiveFinishDialog(text: String){
    let config = new MatDialogConfig();
    config = {
      height: '70%',
      width: '99%',
      panelClass: 'activity-overlay',
      data: text
    };
    this.activityDialog = this.dialog.open(ObjectiveFinishedComponent,config);

    this.activityDialog.afterClosed().subscribe(res => {
      //this.ngOnInit();
      })
  }

}
