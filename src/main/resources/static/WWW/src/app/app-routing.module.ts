import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenuComponent } from './menu/menu.component';
import { ProfileComponent } from './profile/profile.component';
import { MissionsComponent } from './missions/missions.component';
import { RankingComponent } from './ranking/ranking.component';
import { AboutComponent } from './about/about.component';
import { LoginComponent } from './login/login.component';
import { AuthGuardService } from './authentication/auth-guard.service';
import { MissionComponent } from './mission/mission.component';
import { MapComponent } from './map/map.component';
import { ObjectiveComponent } from './objective/objective.component';
import { RegistrationComponent } from './registration/registration.component';
import { MissionCreatorComponent } from './mission-creator/mission-creator.component';



const routes: Routes = [
  {path: 'menu', 
    component: MenuComponent, 
    data:{  animation: 'Menu'},
    canActivate: [AuthGuardService], 
    },
  {path: 'profile', 
    component: ProfileComponent, 
    data:{  animation: 'Profile'},
    canActivate: [AuthGuardService]
    },
  {path: 'missions', 
    component: MissionsComponent, 
    data:{  animation: 'Missions'},
    canActivate: [AuthGuardService]
    },
  {path: 'ranking', component: 
    RankingComponent, 
    data:{  animation: 'Ranking'},
    canActivate: [AuthGuardService]
    },
  {path: 'mission_creator', 
    component: MissionCreatorComponent, 
    data:{  animation: 'Creator'},
    canActivate: [AuthGuardService], },
  {path: 'mission/:mission_id', 
    component: MissionComponent, 
    data:{  animation: 'Mission'},
    canActivate: [AuthGuardService], },   
  {path: 'login', 
    component: LoginComponent,
    data:{  animation: 'Login'}},
  {path: 'objective', 
    component: ObjectiveComponent,
    data:{  animation: 'Mission'},
    canActivate: [AuthGuardService], },
  {path: 'register', 
    component: RegistrationComponent,
    data:{  animation: 'Registration'}},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
