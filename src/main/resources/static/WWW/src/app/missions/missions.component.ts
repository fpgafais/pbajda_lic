import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http/http.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-missions',
  templateUrl: './missions.component.html',
  styleUrls: ['./missions.component.sass']
})
export class MissionsComponent implements OnInit {

  constructor(
    private http  : HttpService,
    private router: Router,
  ) { }

  finished_missions;
  in_progress_missions;
  avaible_missions;
  private loaded = false;
  image_64: string = 'url(data:image/png;base64,';

  ngOnInit() {
    this.http.getUserMissions().subscribe(res => {
      this.finished_missions = res['finished'],
      this.in_progress_missions = res['in_progress'];
      this.avaible_missions = res['avaible'];
      this.loaded = true;
    })
  }

  goToMission(mission_id: number){
    this.router.navigate(['/mission', mission_id]);
  }
}
