import { Component, OnInit } from '@angular/core';
import { MissionCreatorObject } from '../model/mission-creator-object';
import { ObjectiveCreatorObject } from '../model/objective-creator-object';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material';
import { ObjectiveCreatorComponent } from '../objective-creator/objective-creator.component';
import { Objective } from '../model/objective';
import { HttpService } from '../http/http.service';
import { MissionCreatorResponse } from '../model/mission-creator-response';
import { SelectorMatcher } from '@angular/compiler';
import { Router } from '@angular/router';


//https://www.freecodecamp.org/news/how-to-make-image-upload-easy-with-angular-1ed14cb2773b/

class ImageSnippet {
  constructor(public src: string, public file: File) {}
}

@Component({
  selector: 'app-mission-creator',
  templateUrl: './mission-creator.component.html',
  styleUrls: ['./mission-creator.component.sass']
})
export class MissionCreatorComponent implements OnInit {

  mission: MissionCreatorObject;
  objectiveFocus: number = -1;

  missionPhoto: File;

  response: MissionCreatorResponse;

  

  constructor(
    public dialog: MatDialog,
    private http: HttpService,
    private router: Router,
    ) {
    this.mission = new MissionCreatorObject();
    this.response = new MissionCreatorResponse();
   }

  ngOnInit() {
    
  }

  addObjective(){
    this.newObjective();
  }

  photoUpload(){
    //const file: File = this.mission.image;
  }

  openObjectiveCreator(objective: ObjectiveCreatorObject){
    let config = new MatDialogConfig();
    config = {
      height: '99%',
      width: '99%',
      panelClass: 'activity-overlay',
    };
    config.data = {
      objective: objective,
    };

    return  this.dialog.open(ObjectiveCreatorComponent, config);
  }

  newObjective(){
    var objective = new ObjectiveCreatorObject();
    var dialog : MatDialogRef<any> = this.openObjectiveCreator(objective);
    

    dialog.afterClosed().subscribe((response: ObjectiveCreatorObject) => {
      if(response){
        response.objective_number = this.mission.objectives.length + 1;
        this.mission.objectives.push(response);
      }
    })
  }

  editObjective(index: number){
    var dialog : MatDialogRef<any> = this.openObjectiveCreator(this.mission.objectives[index]);

    dialog.afterClosed().subscribe((response: ObjectiveCreatorObject) => {
      if(response)
        this.mission.objectives[index] = response;
    })
  }

  deleteObjective(index: number){
    this.mission.objectives.splice(index, 1);
  }

  unfocusObjective(){
    this.objectiveFocus = -1;
  }

  debug(){
    var objective : ObjectiveCreatorObject = new ObjectiveCreatorObject();
    objective.description = "12 31231782g3 871g2381g 283g1g2 3812g 3g1293fg12 9 731972fg3";
    objective.poly_points = [[25.103491544723507, 47.83279871319485],[-24.629732966423045, 38.61293042384301],
        [39.85199332237243, 17.624912114070312],[25.103491544723507, 47.83279871319485]];
    objective.activity = "{'length': 14}";
    this.mission.objectives.push(objective);
  }

  objectiveOverlay(i: number){
    this.objectiveFocus = i;
  }

  complete(){
    var form = new FormData();
    form.append('mission',JSON.stringify(this.mission));
    if(!this.missionPhoto)
      this.missionPhoto = new File([],"null");
    form.append('image', this.missionPhoto, this.missionPhoto.name);
    this.http.addMission(form).subscribe((res: MissionCreatorResponse) => {
      this.response = res;
      if(res.success){
        this.success(this.response.missionId);
      }
        
    })
  }

  success(missionId: Number){
    setTimeout(() => {
      this.router.navigate(['/mission', missionId]);
    }, 1000)
  }

  onFileChanged(event){
    this.missionPhoto = event.target.files[0];
  }

  swapTop(index: number){
    if(index != 0){
      var objective: ObjectiveCreatorObject = this.mission.objectives[index];
      this.mission.objectives[index] = this.mission.objectives[index - 1];
      this.mission.objectives[index - 1] = objective;
    }
  }

  swapBottom(index: number){
    if(index + 1 != this.mission.objectives.length){
      var objective: ObjectiveCreatorObject = this.mission.objectives[index];
      this.mission.objectives[index] = this.mission.objectives[index + 1];
      this.mission.objectives[index + 1] = objective;
    }
  }

}
