import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GeoLocationService {

  public static longitude:number;
  public static latitude:number;


  constructor() { 
  }

  public static locations = new Observable((observer) => {
    let watchId: number;
  
    // Simple geolocation API check provides values to publish
    if ('geolocation' in navigator) {
      watchId = navigator.geolocation.watchPosition((position: Position) => {
        GeoLocationService.longitude = position['coords']['longitude'];
        GeoLocationService.latitude = position['coords']['latitude'];
        observer.next(position);
      }, (error: PositionError) => {
        observer.error(error);
      },
        {
          enableHighAccuracy: true,
          timeout: 5000,
          maximumAge: 0
        }
      );
    } else {
      observer.error('Geolocation not available');
    }
  
    // When the consumer unsubscribes, clean up data ready for next subscription.
    return {
      unsubscribe() {
        navigator.geolocation.clearWatch(watchId);
      }
    };
  });


}
