import {MAT_DIALOG_DATA, MatDialog} from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-objective-info',
  templateUrl: './objective-info.component.html',
  styleUrls: ['./objective-info.component.sass']
})
export class ObjectiveInfoComponent implements OnInit {
  description: String;
  activityType: String;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog ,
  ) { }
 

  ngOnInit() {
    this.description = this.data['objectiveInfo'];
  }

  close(){
    this.dialog.closeAll();
  }

}
