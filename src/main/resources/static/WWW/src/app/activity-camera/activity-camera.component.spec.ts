import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityCameraComponent } from './activity-camera.component';

describe('ActivityCameraComponent', () => {
  let component: ActivityCameraComponent;
  let fixture: ComponentFixture<ActivityCameraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityCameraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityCameraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
