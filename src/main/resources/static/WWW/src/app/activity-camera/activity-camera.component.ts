import { Component, OnInit, ViewChild, AfterViewInit, ElementRef, Renderer2, OnDestroy } from '@angular/core';
import {  MatDialogRef, throwToolbarMixedModesError } from '@angular/material';

@Component({
  selector: 'app-activity-camera',
  templateUrl: './activity-camera.component.html',
  styleUrls: ['./activity-camera.component.sass']
})
export class ActivityCameraComponent implements OnInit, OnDestroy {
  @ViewChild('video', { static: true }) videoElement: ElementRef;
  @ViewChild('animatethis', {static: false}) overlayAnimation: ElementRef;

  private stream: MediaStream;

  constraints = {
    video: {
        facingMode: "environment",
        //width: { ideal: 600 },
        //height: { ideal:  300}
    }
};

  constructor(
    private renderer: Renderer2,
    public dialog: MatDialogRef<ActivityCameraComponent>,
  ){

  }
  ngOnInit() {
    this.startCamera();
  }

  startCamera() {
    if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) { 
      navigator.mediaDevices.getUserMedia(this.constraints).then(this.attachVideo.bind(this)).catch(this.handleError);
    } else {
        alert('Sorry, camera not available.');
    }
  }

  attachVideo(stream) {
    this.stream = stream;
    this.renderer.setProperty(this.videoElement.nativeElement, 'srcObject', stream);
  }

  handleError(error) {
    console.log('Error: ', error);
  }

  exitPhoto(){

    this.dialog.close(false);
  }

  snapPhoto(){
    this.overlayAnimation.nativeElement.classList.add('cam-overlay-animation')
    setTimeout(() => {
      this.dialog.close(true);
    }, 600);
  }

  ngAfterViewInit(){
    setTimeout(() => {

    }, 1000);
  }

  ngOnDestroy(){
    this.stream.getTracks().forEach(function(track) {
      track.stop();
    })
  }

}
