import{
    trigger,
    transition,
    style,
    query,
    group,
    animateChild,
    animate,
    keyframes,
} from '@angular/animations';

export const menuAnim = 
trigger('routeAnimations', [
    transition('* => Menu', [
        query(':enter, :leave', 
        style({ position: 'fixed',  width: '100%' }), 
        { optional: true }),
        group([
                query(':enter', [
                    style({ transform: 'translateX(-100%)' }), 
                    animate('0.3s ease-in-out', 
                    style({ transform: 'translateX(0%)' }))
                ], { optional: true }),
                query(':leave', [
                    style({ transform: 'translateX(0%)' }),
                    animate('0.3s ease-in-out', 
                    style({ transform: 'translateX(100%)' }))
                    ], { optional: true }),
        ])
    ]),
    transition('Menu => *', [
        query(':enter, :leave', 
             style({ position: 'fixed',  width: '100%' }), 
             { optional: true }),
        group([
             query(':enter', [
                 style({ transform: 'translateX(100%)' }), 
                 animate('0.3s ease-in-out', 
                 style({ transform: 'translateX(0%)' }))
             ], { optional: true }),
             query(':leave', [
                 style({ transform: 'translateX(0%)' }),
                 animate('0.3s ease-in-out', 
                 style({ transform: 'translateX(-100%)' }))
                 ], { optional: true }),
         ])
   ]),
   transition('Missions => Mission', [
    query(':enter, :leave', 
         style({ position: 'fixed',  width: '100%' }), 
         { optional: true }),
    group([
         query(':enter', [
             style({ transform: 'translateX(100%)' }), 
             animate('0.3s ease-in-out', 
             style({ transform: 'translateX(0%)' }))
         ], { optional: true }),
         query(':leave', [
             style({ transform: 'translateX(0%)' }),
             animate('0.3s ease-in-out', 
             style({ transform: 'translateX(-100%)' }))
             ], { optional: true }),
     ])
    ]),
    transition('Mission => Missions', [
        query(':enter, :leave', 
        style({ position: 'fixed',  width: '100%' }), 
            { optional: true }),
        group([
                query(':enter', [
                    style({ transform: 'translateX(-100%)' }), 
                    animate('0.3s ease-in-out', 
                    style({ transform: 'translateX(0%)' }))
                ], { optional: true }),
                query(':leave', [
                    style({ transform: 'translateX(0%)' }),
                    animate('0.3s ease-in-out', 
                    style({ transform: 'translateX(100%)' }))
                    ], { optional: true }),
        ])
        ]),
   transition('Login => *', [
    query(':enter, :leave', 
         style({ position: 'fixed',  width: '100%' }), 
         { optional: true }),
    group([
         query(':enter', [
             style({ transform: 'translateX(100%)' }), 
             animate('0.3s ease-in-out', 
             style({ transform: 'translateX(0%)' }))
         ], { optional: true }),
         query(':leave', [
             style({ transform: 'translateX(0%)' }),
             animate('0.3s ease-in-out', 
             style({ transform: 'translateX(-100%)' }))
             ], { optional: true }),
     ])
]),
]);