import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import 'ol/ol.css';
import {Map, View} from 'ol';
import TileLayer from 'ol/layer/Tile';
import XYZSource from 'ol/source/XYZ';
import {fromLonLat, transform} from 'ol/proj';
import OSMSource from 'ol/source/OSM';
import OSM from 'ol/source/OSM';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import Feature from 'ol/Feature';
import Polygon, {circular} from 'ol/geom/Polygon';
import Point from 'ol/geom/Point';
import { GeoLocationService } from '../geolocation/geo-location.service';
import Control from 'ol/control/Control';
import {Style, Icon, Fill, Stroke} from 'ol/style';
import Kompas from 'kompas';
import {defaults as defaultInteractions, PinchZoom, DragRotateAndZoom, DragPan} from 'ol/interaction';
import { createOrUpdateFromCoordinates, coordinateRelationship } from 'ol/extent';
import { pointerMove } from 'ol/events/condition';


var map: Map;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.sass']
})
export class MapComponent implements OnInit, OnDestroy {
  @Input() poly_points;
  @Input() coordinates;
  @Input() activityCoords;
  @Output() intersectOutput = new EventEmitter<boolean>();
  @Output() compassOutput = new EventEmitter<number>();


  map: Map;
  source: any;
  vectorSource: VectorSource;
  polygonFeatureId: number;
  insidePolygon = false;

  private locationsSubscription;
  coords = null;
  

  constructor(
    private geoLocation : GeoLocationService,
  ) { 
  }

  ngOnInit() {
    this.initMap();
    this.initUserMarker();
    this.initLocationButton();
    this.initObjectiveLocationButton();
    this.initVectorLayer();
    this.addPolygon(this.poly_points, this.coordinates, this.activityCoords);
    this.startGeoLocation();
  }

  ngOnDestroy(){
    this.closegeoLocation();
  }

  initMap(){

    this.map = new Map({
      target: 'map-container',
      interactions: defaultInteractions().extend([
        new DragPan(),
        new PinchZoom(),
      ]),
      layers: [
        new TileLayer({
          source: new OSMSource()
        })
      ],
      view: new View({
        center: fromLonLat([0, 0]),
        zoom: 2,
        enableRotation: false,
      })
    });

  }

  initUserMarker(){
 
    this.source = new VectorSource();
    const layer = new VectorLayer({
      source: this.source
    });
    const style = new Style({
      fill: new Fill({
        color: 'rgba(0, 0, 255, 0.2)'
      }),
      stroke: new Stroke({
        color: 'rgba(0, 0, 255, 0.6)',
        width: 2,
      }),
      image: new Icon({
        src: './assets/marker.svg',
        scale: 0.08,
        rotateWithView: true
      })
    });
    layer.setStyle(style);

    this.map.addLayer(layer);

    const compass = new Kompas();
    compass.watch();
    
    compass.on('heading', (heading) => {
      this.compassOutput.emit(heading);
      style.getImage().setRotation(Math.PI / 180 * heading);
      layer.setStyle(style);
    });
  }

  initLocationButton(){
    const locate = document.createElement('div');
    locate.className = 'ol-control ol-unselectable locate';
    locate.innerHTML = '<button title="Locate me">◎</button>';
    locate.addEventListener('click', () => this.centerMap());
    this.map.addControl(new Control({
      element: locate
    }));
  }

  initObjectiveLocationButton(){
    const locate = document.createElement('div');
    locate.className = 'ol-control ol-unselectable locate-objective';
    locate.innerHTML = '<button title="Locate objective">🏳</button>';
    locate.addEventListener('click', () => this.centerMap(this.coordinates));
    this.map.addControl(new Control({
      element: locate
    }));
  }


  initVectorLayer(){
    this.vectorSource = new VectorSource();
    var vectorLayer = new VectorLayer({
      source: this.vectorSource
    });
    this.map.addLayer(vectorLayer);
  }


  centerMap(coords: [] = null){
    if (!this.source.isEmpty()) {
      if(coords == null){
        this.map.getView().fit(this.source.getExtent(), {
          maxZoom: 18,
          duration: 500
        });
      }
      else{
        this.map.getView().setCenter(transform(coords, 'EPSG:4326', 'EPSG:3857'));
        this.map.getView().setZoom(16);
      }
    }
  }




  startGeoLocation(){
   this.locationsSubscription = GeoLocationService.locations.subscribe((position: Position)=>{
      this.coords = position['coords'];
      const coords = [position.coords.longitude, position.coords.latitude];
      const accuracy = circular(coords, position.coords.accuracy);
      var intersects = this.checkIfIntersects(coords);
      if(this.insidePolygon != intersects){
        this.insidePolygon = intersects;
        this.intersectOutput.emit(this.insidePolygon);
      }
      this.source.clear(true);
      this.source.addFeatures([
        new Feature(accuracy.transform('EPSG:4326', this.map.getView().getProjection())),
        new Feature(new Point(fromLonLat(coords)))
      ]);
    })
  }

  addPolygon(poly_points: [][], coordinates: [], activityCoords: []){
    var polyFeature = new Feature({
      geometry: new Polygon([poly_points]),
    })
    
    var iconFeature = new Feature({
      geometry: new Point(coordinates),
    })


    var iconStyle = new Style({
      image: new Icon({
        src: './assets/marker_objective.svg',
        scale: 0.06,
      })
    })


    iconFeature.setStyle(iconStyle);

    polyFeature.setId(1);//it will be used on intersection check
    this.polygonFeatureId = <number>polyFeature.getId();

    iconFeature.getGeometry().transform('EPSG:4326', 'EPSG:3857');
    polyFeature.getGeometry().transform('EPSG:4326', 'EPSG:3857');

    this.vectorSource.clear(true);
    this.vectorSource.addFeature(polyFeature);
    this.vectorSource.addFeature(iconFeature);

    if(activityCoords){
      var activityFeature = new Feature({
        geometry: new Point(activityCoords),
      });
      var iconStyleCam = new Style({
        image: new Icon({
          src: './assets/marker_camera.svg',
          scale: 0.06,
        })
      });
      activityFeature.setStyle(iconStyleCam);
      activityFeature.getGeometry().transform('EPSG:4326', 'EPSG:3857');
      this.vectorSource.addFeature(activityFeature);
    }

    //this.vectorSource.clear()
  }

  clearPolygons(){
    this.vectorSource.clear(true);
  }

  checkIfIntersects(coords){

    var poly = this.vectorSource.getFeatureById(this.polygonFeatureId).getGeometry();


    return poly.intersectsCoordinate(fromLonLat(coords));
  }

  closegeoLocation(){
    if(this.locationsSubscription)
      this.locationsSubscription.unsubscribe();
  }

}
