import { Component, OnInit, Output } from '@angular/core';
import { HttpService } from '../http/http.service';
import { UserData } from '../model/user-data';
import { Ranking } from '../model/ranking';
import { LvlClass } from '../model/lvl-class';
import { LvlCalculatorService } from '../lvl/lvl-calculator.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass']
})
export class ProfileComponent implements OnInit {


  constructor(
    private http : HttpService,
    private lvlCalc: LvlCalculatorService,
  ) { }

  userData : UserData;
  ranking : Ranking;
  finishedMissions : Number;
  lvl : number;
  next_lvl : number;
  progress: number;
  progress_base: number;

  isLoaded = false;

  ngOnInit() {
    this.isLoaded = false;
    this.getUserProfile();
  }

  getUserProfile(){
    this.http.getuserProfile().subscribe(res => {
      this.userData = res['userData'];
      this.ranking = res['ranking'];
      this.finishedMissions = res['finishedMissions'];
      this.calculateLvl();
      this.isLoaded = true;
    })
  }

  calculateLvl(){   
    var lvlObject : LvlClass = this.lvlCalc.calculateLvl(this.ranking.points);
    this.lvl = lvlObject.lvl;
    this.progress_base = lvlObject.lvlBasePoints;
    this.next_lvl = lvlObject.requiredForNext;  
  }

  calculateProgressbar(){
    let width = ((this.ranking.points - this.progress_base) / this.next_lvl) * 100;
    console.log("calculated " + width);
    let element: any = document.getElementById('progress');
    element.style.width = width.toString() + '%';
  }



}
