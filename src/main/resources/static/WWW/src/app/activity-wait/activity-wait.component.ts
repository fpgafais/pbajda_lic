import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { interval } from 'rxjs';

@Component({
  selector: 'app-activity-wait',
  templateUrl: './activity-wait.component.html',
  styleUrls: ['./activity-wait.component.sass']
})
export class ActivityWaitComponent implements OnInit {
  start: number;
  end: number;

  progress: number = 0;
  length: number = 1;

  inter: any;
  intervalSubscribe;

  loadBarProcent: String = '0%';
  procentCalculated: number = 0;

  animation = '';
  shadow_def: String = '0px 3px 8px #aaa, inset 0px 2px 3px #fff';
  animation_press = 'expand-activity-button 1.5s  forwards';
  

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialogRef<ActivityWaitComponent>,
  ) { }

  ngOnInit() {
    this.length = this.data['length'];
    this.inter = interval(100);
  }


  closeActivity(){

    var response = {
      start: this.start,
      end: this.end
    }

    setTimeout(() => {
      this.dialog.close(response);
    }, 600);

  }
  /*
    0                   200
      ------------------

      200

  */


  buttonStart(){
    this.animation = this.animation_press;
    if(this.procentCalculated < 100){
      this.start = Date.now();

      this.intervalSubscribe = this.inter.subscribe(n => {
        this.procentCalculated = (n/((this.length * 10) / 100))
        if(this.procentCalculated <= 100)
          this.loadBarProcent =  this.procentCalculated.toString() +'%';
      })
    }
  }

  buttonEnd(){
    this.animation = '';

    this.end = Date.now();

    this.intervalSubscribe.unsubscribe();

    if(this.procentCalculated >= 100){
      this.closeActivity();
    }
  }

  

}
