import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityWaitComponent } from './activity-wait.component';

describe('ActivityWaitComponent', () => {
  let component: ActivityWaitComponent;
  let fixture: ComponentFixture<ActivityWaitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityWaitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityWaitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
