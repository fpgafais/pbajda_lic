import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AppComponent } from '../app.component';
import { UserData } from '../model/user-data';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // BASE_PATH: 'http://localhost:8080'
  USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser'

  public username: String;
  public password: String;


  constructor(
    private http: HttpClient,
    ) {
  }

  public jwtHelper: JwtHelperService = new JwtHelperService();

  public isAuthenticated(): boolean {//jwt-authentication using route guards
    const token = localStorage.getItem('token');

    return !this.jwtHelper.isTokenExpired(token);
  }


  authenticationService(username: String, password: String) {
    const data = {'username':username, 'password':password};
    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json') };
    return this.http.post(environment.IP + "user/authenticate",data, config).pipe(map((res) => {
        this.username = username;
        this.password = password;
        this.registerSuccessfulLogin(res);
        console.log(res);
      }));
  }

  createBasicAuthToken(username: String, password: String) {
    return 'Basic ' + window.btoa(username + ":" + password)
  }

  registerSuccessfulLogin(token) {
    sessionStorage.setItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME, token);
    localStorage.setItem('token', token['jwt'])
  }

  logout() {
    sessionStorage.removeItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME);
    localStorage.removeItem('token');
    this.username = null;
    this.password = null;
  }

  isUserLoggedIn() {
    let user = sessionStorage.getItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME)
    if (user === null) return false
    return true
  }

  getLoggedInUserName() {
    let user = sessionStorage.getItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME)
    if (user === null) return ''
    return user
  }


  getToken(){
    return 'Bearer ' + localStorage.getItem('token');
  }

  getUserData(){
    return this.http.get(environment.IP + 'userdata');
  }




}