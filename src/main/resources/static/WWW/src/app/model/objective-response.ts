import { Objective } from './objective';

export class ObjectiveResponse {
    success: Boolean;
    responseText: String;
    objective: Objective;
}
