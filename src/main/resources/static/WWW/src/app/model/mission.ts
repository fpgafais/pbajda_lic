export class Mission {
    mission_id: Number;
    name: String;
    description: String;
    picture: File; //change
    user_id : Number;
    creation_date: Date;
    odification_date: Date;
    active: Boolean;
    difficulty_id: Number;
    objective_count: Number;
    points : Number;
    minimum_points : Number;
}
