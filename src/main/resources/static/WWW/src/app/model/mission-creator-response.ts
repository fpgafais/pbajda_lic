import { MissionCreatorErrors } from './mission-creator-errors';

export class MissionCreatorResponse {
    success: Boolean;
    errors: MissionCreatorErrors;
    missionId: Number;

    constructor(){
        this.success = false;
        this.errors = new MissionCreatorErrors();
    }
}
