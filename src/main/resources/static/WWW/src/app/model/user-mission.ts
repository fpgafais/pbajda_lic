import { Mission } from './mission';


export class UserMission {
    mission_id: Number;
    objective_progress : Number;
    mission : Mission;
}
