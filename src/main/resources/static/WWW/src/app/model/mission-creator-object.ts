import { ObjectiveCreatorObject } from './objective-creator-object';

export class MissionCreatorObject {
    name : String;
    description : String;
    missionDifficulty: Number;

    minimumLvl: Number;
    objectives : ObjectiveCreatorObject[];

    constructor(){
        this.name = "",
        this.description = "";
        this.minimumLvl = 1;
        this.missionDifficulty = 1;
        this.objectives = [];
    }
}
