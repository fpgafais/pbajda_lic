export class RegistrationResponse {
    success: Boolean;

    errors: {
        emailTaken: Boolean;

        invalidEmail: Boolean;
    
        passwordsDoNotMatch: Boolean;
    
        emptyField: Boolean;
    }

    constructor(){
        this.success =  false;
        this.errors = {
            emailTaken:false,
            invalidEmail: false,       
            passwordsDoNotMatch: false,   
            emptyField: false,
        }
        
    }

}
