export class Objective  {
    objective_id: Number;
    description: String;
    mission_id: Number;
    type: String;
    coordinates: [];
    poly_points: [];
    objective_number: Number;
}
