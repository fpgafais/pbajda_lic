import { Mission } from './mission';

export class MissionResponse {
    mission_id: Number;
    mission: Mission;
    status: String;
}
