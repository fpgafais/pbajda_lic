export class Registration {
    username: string;
    email: string;
    password: string;
    matchingPassword: string;

    constructor(){
        this.username= '';
        this.email='';
        this.password='';
        this.matchingPassword='';
    }
}
