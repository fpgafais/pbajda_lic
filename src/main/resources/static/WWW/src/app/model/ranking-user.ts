export class RankingUser {
    position: Number;
    username: String;
    points: Number;
}
