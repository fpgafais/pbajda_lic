export class MissionCreatorErrors {
    emptyField: Boolean;
    tooLongName: Boolean;
    tooLongDescription: Boolean;
    wrongLvl: Boolean;
    wrongDifficulty: Boolean;
    imageTooLarge: Boolean;
    objectiveError: Boolean;

    constructor(){
        this.emptyField = false;
        this.tooLongName = false;
        this.wrongLvl = false;
        this.wrongDifficulty = false;
        this.imageTooLarge = false;
        this.objectiveError = false;
    }
}
