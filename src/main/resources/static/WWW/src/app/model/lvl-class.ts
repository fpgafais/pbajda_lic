export class LvlClass {
    lvl: number;
    requiredForNext: number;
    lvlBasePoints: number;

    constructor(lvl : number, requiredForNext : number, lvlBasePoints : number){
        this.lvl = lvl;
        this.requiredForNext = requiredForNext;
        this.lvlBasePoints = lvlBasePoints;
    }
}
