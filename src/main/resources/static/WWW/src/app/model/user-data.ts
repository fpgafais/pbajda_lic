export class UserData {
    user_id : Number;
    username: String;
    email: String;
    creation_date: Date;
    modification_date: Date;
    roles: String;
    active: Boolean;
}
