export class ObjectiveCreatorObject {
    description: String;
    type: String;
    coordinates: number[];
    poly_points: number[][];
    objective_number: Number;
    activity: string;

    constructor(){
        this.description = "";
        this.type = "type_1";
        this.coordinates = [];
        this.poly_points = [];
        this.objective_number = 0;
        this.activity = null;
    }
}
