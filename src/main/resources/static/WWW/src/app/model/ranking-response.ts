import { RankingUser } from './ranking-user';

export class RankingResponse {
    ranking: RankingUser[];
    user: RankingUser;
}
