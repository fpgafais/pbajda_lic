import { Directive, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Directive({
  selector: '[ngInit]'
})
export class NgInitDirectiveDirective implements OnInit {

  @Output()
  ngInit: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
      this.ngInit.emit();
  }

}
