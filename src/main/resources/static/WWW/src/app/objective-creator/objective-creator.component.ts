import { Component, OnInit, Inject } from '@angular/core';
import { ObjectiveCreatorObject } from '../model/objective-creator-object';
import {Map, View, Feature} from 'ol';
import { defaults as defaultInteractions, DragPan, PinchZoom, Draw, Snap, Extent } from 'ol/interaction';
import TileLayer from 'ol/layer/Tile';
import { fromLonLat, transform } from 'ol/proj';
import OSMSource from 'ol/source/OSM';
import VectorSource from 'ol/source/Vector';
import VectorLayer from 'ol/layer/Vector';
import GeometryType from 'ol/geom/GeometryType';
import Geometry from 'ol/geom/Geometry';
import Point from 'ol/geom/Point';
import { Style, Icon } from 'ol/style';
import { getCenter } from 'ol/extent';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ActivityType2 } from '../model/activity-type2';
import { ActivityType1 } from '../model/activity-type1';
import { isEmpty } from 'rxjs/operators';
import { Coordinate } from 'ol/coordinate';
import Polygon from 'ol/geom/Polygon';

@Component({
  selector: 'app-objective-creator',
  templateUrl: './objective-creator.component.html',
  styleUrls: ['./objective-creator.component.sass']
})
export class ObjectiveCreatorComponent implements OnInit {

  objective: ObjectiveCreatorObject;

  map: Map;
  vectorSource: VectorSource;
  cameraPinSource: VectorSource;
  draw: Draw;
  snap: Snap;
  polygonGeometry: Geometry;
  polygonCoordinates: number[];

  pinAdding: Boolean = false;
  polygonDrawn: Boolean = false;

  waitTimeText: string;
  activityCoordinate: number[];
  center: number[];

  error: Boolean = false;
  

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialogRef<ObjectiveCreatorComponent>,
  ) { 
    this.objective = data['objective'];
  }

  ngOnInit() {
    this.initMap();
    this.initLayers();
    if(this.objective.poly_points && this.objective.poly_points.length != 0)
      this.addPolygon(this.objective.poly_points)
    if(this.objective.activity){
      var activity = JSON.parse(this.objective.activity);
      if(this.objective.type == 'type_1'){
        this.waitTimeText = activity['length'];
      }       
      else if(this.objective.type == 'type_2'){
        let pinCoords = transform(activity['object_coordinates'],'EPSG:4326', 'EPSG:3857')
        this.addPhotoPin(pinCoords);
      }
    }
      
  }


  initMap(){
    this.map = new Map({
      target: 'map-container',
      interactions: defaultInteractions().extend([
        new DragPan(),
        new PinchZoom(),
      ]),
      layers: [
        new TileLayer({
          source: new OSMSource()
        })
      ],
      view: new View({
        center: fromLonLat([0, 0]),
        zoom: 2,
        enableRotation: false,
      })
    });

    this.map.on('singleclick', (event)=>{
      if(this.objective.type == 'type_2' && this.pinAdding){
        this.addPhotoPin(event.coordinate);
      }
    })
  }

  

  initLayers(){
    this.vectorSource = new VectorSource();
    var vectorLayer = new VectorLayer({
      source: this.vectorSource
    });
    this.map.addLayer(vectorLayer);

    this.cameraPinSource = new VectorSource();
    var vectorLayer = new VectorLayer({
      source: this.cameraPinSource
    });
    this.map.addLayer(vectorLayer);
  }

  addPolygon(poly_points: number[][]){
    var polyFeature = new Feature({
      geometry: new Polygon([poly_points]),
    });

    polyFeature.getGeometry().transform('EPSG:4326', 'EPSG:3857');

    this.vectorSource.addFeature(polyFeature);

    this.putCenterPin();

    this.polygonDrawn = true;
  }

  drawPolygon(){
    this.map.removeInteraction(this.draw);
    this.map.removeInteraction(this.snap);
    this.pinAdding = false;
    this.addInteractions()
  }

  clearPolygon(){
    this.vectorSource.clear();
    this.polygonDrawn = false;
  }

  addInteractions() {
    this.draw = new Draw({
      source: this.vectorSource,
      type: GeometryType.POLYGON,
    });
    this.draw.on('drawend', () =>{
      this.polygonDrawn = true;
      this.map.removeInteraction(this.draw);

      setTimeout(() => {  this.putCenterPin(); }, 100);
    })
    this.map.addInteraction(this.draw);
    this.snap = new Snap({
      source: this.vectorSource,
    });
    this.map.addInteraction(this.snap);
  }

  readPolygonCoordds(){ 
    var polygonCoordinates : Coordinate;

    if(this.vectorSource.getFeatures().length != 0){
      var polygonGeometry = this.vectorSource.getFeatures()[0].getGeometry();
      var point: Point = <Point>polygonGeometry;
      polygonCoordinates = point.getCoordinates();
    }

    return polygonCoordinates[0];
  }

  putCenterPin(){
    this.center = this.getCenterOfPolygon();

    this.addPin(this.center,'marker_objective', true);
  }

  getCenterOfPolygon(){
    var center: number[];

    if(this.vectorSource.getFeatures().length != 0){
      var polygonGeometry = this.vectorSource.getFeatures()[0].getGeometry();
      var extent = polygonGeometry.getExtent();
      center = getCenter(extent)
    }

    return center;
  }


  addPin(coordinates: number[], pinName: String, arealayer: Boolean){
    var iconFeature = new Feature({
      geometry: new Point(coordinates),
    })

    var iconStyle = new Style({
      image: new Icon({
        src: './assets/'+pinName+'.svg',
        scale: 0.06,
      })
    })

    iconFeature.setStyle(iconStyle);
    if(arealayer)
      this.vectorSource.addFeature(iconFeature);
    else
      this.cameraPinSource.addFeature(iconFeature);
  }

  addPhotoInteraction(){
    this.pinAdding = !this.pinAdding;
  }

  addPhotoPin(coordinates: number[]){
    this.activityCoordinate = transform(coordinates,'EPSG:3857','EPSG:4326');
    this.cameraPinSource.clear();
    this.addPin(coordinates,'marker_camera',false);
  }

  onTypeChange(){
    this.activityCoordinate = [];
    this.cameraPinSource.clear();
  }

  dialogCancel(){
    this.dialog.close();
  }

  dialogAccept(){
    this.error = false;

    var waitTime: number = parseInt(this.waitTimeText)


    var poly_points  = this.readPolygonCoordds();

    this.objective.poly_points = [];

    for(let i = 0; i < poly_points['length']; i++){
      this.objective.poly_points.push(transform(poly_points[i],'EPSG:3857','EPSG:4326'));
    }
    //this.objective.poly_points.forEach((element : number[])=> {
    //  transform(element ,'EPSG:3857','EPSG:4326');
    //});

    this.objective.coordinates = transform(this.getCenterOfPolygon(), 'EPSG:3857','EPSG:4326');

    if(!this.objective.poly_points)
      this.error = true;

    if(this.objective.type == "type_1"){
      if(!waitTime || typeof waitTime != 'number' || waitTime < 2 || waitTime > 180)
        this.error = true
      this.objective.activity = JSON.stringify(<ActivityType1>{length: waitTime});
    }
    else if(this.objective.type == "type_2"){
      if(!this.activityCoordinate || this.activityCoordinate.length != 2)
        this.error = true
      this.objective.activity =  JSON.stringify(<ActivityType2>{object_coordinates: this.activityCoordinate});
    }

    if(!this.error)
      this.dialog.close(this.objective)
  }
}
