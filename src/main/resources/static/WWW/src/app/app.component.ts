import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { RouterOutlet, ActivatedRoute, Router } from '@angular/router';
import { menuAnim } from './route-animation';
import { AuthService } from './authentication/auth.service';
import { UserData } from './model/user-data';
import { HttpService } from './http/http.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./app.component.sass'],
  animations: [
    menuAnim
  ]
})
export class AppComponent {
  title = 'WWW';

  userData: UserData;

  prepareRoute(outlet: RouterOutlet){
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }

  isLoggedIn = false;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthService,
    private http: HttpService) { }

  ngOnInit() {
    this.isLoggedIn = this.authenticationService.isUserLoggedIn();
    if(this.isLoggedIn){
      this.http.getUserData().subscribe((res: UserData) => {
        HttpService.user_id = res.user_id;
        this.userData = res;
      })
    }
  }

}

