import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { UserData } from '../model/user-data';
import { environment } from '../../environments/environment';
import { Registration } from '../model/registration';
import {InterceptorSkipHeader} from  '../http-interceptor.service';
import { MissionCreatorObject } from '../model/mission-creator-object';
import { Mission } from '../model/mission';
import { AuthService } from '../authentication/auth.service';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  static user_id : Number;


  constructor(
    private http: HttpClient,
    private authenticationService: AuthService,
    
  ) { }

  getUserData(){
    return this.http.get(environment.IP + 'userdata');
  }

  getuserProfile(){
    return this.http.get(environment.IP + 'getUserProfile');
  }
  
  getUserMissions(){
    return this.http.get(environment.IP + 'getUserMissions');
  }

  getMission(user_id, mission_id){
    let params = new HttpParams().set("userId",user_id).set("missionId", mission_id);
    return this.http.get(environment.IP + 'getMission', {params: params});
  }

  getObjective(mission_id){
    let params = new HttpParams().set("missionId", mission_id);
    return this.http.get(environment.IP + 'getObjective', {params: params});
  }

  sendRegistrationForm(userDetails: Registration){
    return this.http.post(environment.IP + 'user/registration', userDetails);
  }

  addMission(form: FormData){
    var headers = new HttpHeaders().set(InterceptorSkipHeader, '');
    return this.http.post(environment.IP + 'addMission', form, {headers});
  }

  getRanking(){
    return this.http.get(environment.IP + 'getRanking');
  }

}
