import { Component, OnInit, SystemJsNgModuleLoader, OnDestroy } from '@angular/core';


declare var ol: any;//map test

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.sass'],
})
export class AboutComponent implements OnInit {

  objective: Boolean = false;
  
  constructor() {

  }


  ngOnInit() {

  }

  addObjective(){
    this.objective = !this.objective;
  }



}
