import { TestBed } from '@angular/core/testing';

import { LvlCalculatorService } from './lvl-calculator.service';

describe('LvlCalculatorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LvlCalculatorService = TestBed.get(LvlCalculatorService);
    expect(service).toBeTruthy();
  });
});
