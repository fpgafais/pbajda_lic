import { Injectable } from '@angular/core';
import { LvlClass } from '../model/lvl-class';

@Injectable({
  providedIn: 'root'
})
export class LvlCalculatorService {

  constructor() { }

  calculateLvl(points: number){
    var lvl = 1;
    var next_lvl = 100;
    var points_req = 100;
    let last = 0;
    while(points_req < points){
      last = points_req;
      if(lvl < 5){
        points_req += next_lvl * 2;
        next_lvl = next_lvl * 2; 
      }
      else{
        points_req += next_lvl * 1.25;
        next_lvl = next_lvl * 1.25;
      }
      lvl += 1;
      console.log(lvl, next_lvl, last, points, points_req)
    }      

    return new LvlClass(lvl, next_lvl, last);
  }

  calculatePoints(lvl: number){//Delete
    if(lvl == 1)
      return 1;

    var points_req = 100;
    for(let i = 2; i < lvl; i++){
      if(lvl < 5){
        points_req += points_req * 2;
      }
      else{
        points_req += points_req * 1.5;
      }
    }

    return points_req;
  }
}
