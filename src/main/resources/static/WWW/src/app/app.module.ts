import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { ProfileComponent } from './profile/profile.component';
import { MissionsComponent } from './missions/missions.component';
import { RankingComponent } from './ranking/ranking.component';
import { AboutComponent } from './about/about.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './login/login.component';
import { HttpInterceptorService } from './http-interceptor.service';
import { FormsModule } from '@angular/forms';
import { NgInitDirectiveDirective } from './directive/ng-init-directive.directive';
import { MissionComponent } from './mission/mission.component';
import { WebSocketClientService } from './websocket/web-socket-client.service';
import { InjectableRxStompConfig, RxStompService, rxStompServiceFactory } from '@stomp/ng2-stompjs';
import { myRxStompConfig } from './my-rx-stomp.config';
import { MapComponent } from './map/map.component';
import { ObjectiveComponent } from './objective/objective.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ActivityCameraComponent } from './activity-camera/activity-camera.component';
import { MatDialogModule } from '@angular/material/dialog';
import { ObjectiveInfoComponent } from './objective-info/objective-info.component';
import { ActivityInfoComponent } from './activity-info/activity-info.component';
import { ActivityWaitComponent } from './activity-wait/activity-wait.component';
import { RegistrationComponent } from './registration/registration.component';
import { ObjectiveFinishedComponent } from './objective-finished/objective-finished.component';
import { MissionCreatorComponent } from './mission-creator/mission-creator.component';
import { ObjectiveCreatorComponent } from './objective-creator/objective-creator.component';
import { MatFormFieldModule, MatSelectModule } from '@angular/material';


@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    ProfileComponent,
    MissionsComponent,
    RankingComponent,
    AboutComponent,
    LoginComponent,
    NgInitDirectiveDirective,
    MissionComponent,
    MapComponent,
    ObjectiveComponent,
    ActivityCameraComponent,
    ObjectiveInfoComponent,
    ActivityInfoComponent,
    ActivityWaitComponent,
    RegistrationComponent,
    ObjectiveFinishedComponent,
    MissionCreatorComponent,
    ObjectiveCreatorComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    FlexLayoutModule,
    MatDialogModule,
    MatFormFieldModule,
    MatSelectModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    },
    WebSocketClientService,
    {
      provide: InjectableRxStompConfig,
      useValue: myRxStompConfig
    },
    {
      provide: RxStompService,
      useFactory: rxStompServiceFactory,
      deps: [InjectableRxStompConfig]
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    ActivityCameraComponent, 
    ObjectiveInfoComponent,
    ActivityInfoComponent,
    ActivityWaitComponent,
    ObjectiveFinishedComponent,
    ObjectiveCreatorComponent,
  ],
})
export class AppModule { }
