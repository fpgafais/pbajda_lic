import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from './authentication/auth.service';

export const InterceptorSkipHeader = 'X-Skip-Interceptor';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {

    constructor(private authenticationService: AuthService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this.authenticationService.isUserLoggedIn() && req.url.indexOf('basicauth') === -1) {
            var authReq;
            if (req.headers.has(InterceptorSkipHeader)) {
                authReq = req.clone({
                    headers: new HttpHeaders({
                        //'Content-Type': 'multipart/form-data',
                        'Authorization': this.authenticationService.getToken(),
                    })
                });
            }
            else{
                authReq = req.clone({
                    headers: new HttpHeaders({
                        'Content-Type': 'application/json',
                        'Authorization': this.authenticationService.getToken(),
                    })
                });
            }

            return next.handle(authReq);
        } else {
            return next.handle(req);
        }
    }
}
