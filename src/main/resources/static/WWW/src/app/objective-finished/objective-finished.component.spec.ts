import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjectiveFinishedComponent } from './objective-finished.component';

describe('ObjectiveFinishedComponent', () => {
  let component: ObjectiveFinishedComponent;
  let fixture: ComponentFixture<ObjectiveFinishedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObjectiveFinishedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjectiveFinishedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
