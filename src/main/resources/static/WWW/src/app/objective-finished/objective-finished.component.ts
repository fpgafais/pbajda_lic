import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-objective-finished',
  templateUrl: './objective-finished.component.html',
  styleUrls: ['./objective-finished.component.sass']
})
export class ObjectiveFinishedComponent implements OnInit {

  text: String = '';

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: String,
    public dialog: MatDialogRef<ObjectiveFinishedComponent>,
  ) { }

  ngOnInit() {
    this.text = this.data;
  }

  closeDialog(){
    this.dialog.close();
  }
}
